import os
import sys

import encode

def make_fastq_name(json):
    """Creates an appropriate file name for both single and paired end fastq files"""
    name = json['accession']
    if 'paired_with' in json:
        rep = json['replicate']['biological_replicate_number']
        pair = json['paired_end']
        outname = f"P{pair}_R{rep}_{name}.fastq.gz"
    else:
        outname = f'{name}.fastq.gz'
    return outname

def download_fastq(json, expdir):
    """Downloads a single fastq file to and from appropriate locations"""
    outname = make_fastq_name(json)
    url = f"https://www.encodeproject.org{json['href']}"
    full_outpath = os.path.join(expdir, outname)
    encode.Experiment.stream_file(url, full_outpath)
    print(f"Done downloading {json['href']}\n.")
    return outname

def save_star_file_string(files, outdir):
    """Creates a string to be passed to STAR's --readFilesIn parameter"""
    files.sort()
    paired = files[0][0] == 'P'
    if paired:
        p1 = ','.join(files[:len(files)//2])
        p2 = ','.join(files[len(files)//2:])
        string = f'{p1} {p2}'
    else:
        string = ','.join(files)
    with open(os.path.join(outdir, 'star_str.txt'), 'w') as outfile:
        outfile.write(string)

def process_experiment(exp_id, outdir):
    """Finds, downloads, and processes all fastq files in experiment"""
    print(f'Starting {exp_id}.\n')
    exp = encode.Experiment(exp_id)
    expdir = os.path.join(outdir, exp_id)
    if os.path.isdir(expdir): # Do not redownload
        return
    os.makedirs(expdir)

    fastq_ls = exp.filter_filetype({'file_format': 'fastq'})
    if not fastq_ls: #A couple of experiments are empty
        print(f'Error: {exp_id} is empty.')
        return

    fq_names = [download_fastq(f, expdir) for f in fastq_ls]
    save_star_file_string(fq_names, expdir)
    print(f'Done with {exp_id}!!\n')

def main(url, outdir):
    """Processes each experiment found in json data returned by url"""
    search = encode.Search(url)
    search.run()
    for exp_id in search.gen_experiment_uids():
        process_experiment(exp_id, outdir)

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
