# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 15:28:17 2016

@author: jessime

Programatically search/filter/download data from ENCODE.
https://www.encodeproject.org/search/
"""

import requests
import json

from os import makedirs
from os.path import join, isdir, basename

#from my_tqdm import my_tqdm

class Experiment:
    """Download experimental files from Encode

    Encode's site for selecting your data provides a nice GUI, and is more usable than doing things programmatically.
    So, start by selecting a url that represents the experiments that you're interested in downloading.

    Parameters
    ----------
    uid : str (default=None)
        ENCODE's unique identifier for an experiment
    outdir : str (default=None)

    Examples
    --------
    1.
    """
    def __init__(self, uid=None, outdir='', save_by='target'):
        self.uid = uid
        self.outdir = outdir
        self.save_by = save_by

        self.url = f'https://www.encodeproject.org/experiments/{self.uid}/'
        self.HEADERS = {'accept': 'application/json'}
        self.json = requests.get(self.url, headers=self.HEADERS).json()

    # def make_outfile_path(self, filename):
    #     if self.save_by == 'target':
    #         outdir = join(self.outdir, self.experiment_json[self.save_by]['label'])
    #     else:
    #         outdir = join(self.outdir, self.experiment_json[self.save_by])
    #     if not isdir(outdir):
    #         makedirs(outdir)
    #     path = join(outdir, basename(filename))
    #     return path

    @classmethod
    def stream_file(self, url, outfile):
        response = requests.get(url, stream=True)
        with open(outfile, 'wb') as outfile:
            for chunk in response:
                outfile.write(chunk)

    def filter_filetype(self, file_filters):
        """Get files that match criteria for different parameters

        Parameters
        ----------
        file_filters : {str:str}
            Keys are keys in dicts of self.json['files'], i.e. parameters to match on.
            Values are possible values of dicts of self.json['files'], i.e. the correct criteria
            Example: To filter for fastq files pass {'file_format': 'fastq'}
            Note: If a parameter is not in file data, file will not pass filter

        Returns
        -------
        filtered : list
            Files passing filter criteria
        """
        filtered = []
        for f in self.json['files']:
            for parameter, criteria in file_filters.items():
                if parameter in f:
                    if f[parameter] == criteria:
                        filtered.append(f)
        return filtered
    #
    # def save_data_files(self):
    #     files = self.experiment_json['files']
    #     files = self.apply_file_filters(files)
    #     hrefs = [f['href'] for f in files]
    #     hrefs = [h.strip('/') for h in hrefs]
    #     for href in my_tqdm()(hrefs, leave=False):
    #         outfile = self.make_outfile_path(href)
    #         self.stream_file(join(self.base_url, href), outfile)

class Search:
    """
    Parameters
    ----------
    url : str (default=None)
        Filtered results from ENCODE. For example, the link for human ChIP-seq experiments is:
        https://www.encodeproject.org/search/?type=Experiment&assay_title=ChIP-seq&replicates.library.biosample.donor.organism.scientific_name=Homo+sapiens

    """
    def __init__(self, url=None):
        self.url = url

        self.search_results_json = None
        self.HEADERS = {'accept': 'application/json'}

    def gen_experiment_uids(self):
        "Generate unique experiment ids from search results"
        for experiment in self.search_results_json['@graph']:
            uid = experiment['@id'].split('/')[2]
            yield uid

    def run(self):
        """Store json data for a search of a set of experiments"""
        self.search_results_json = requests.get(self.url, headers=self.HEADERS).json()
