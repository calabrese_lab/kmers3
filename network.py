# -*- coding: utf-8 -*-
"""
Created on Tue May 26 11:57:25 2015

@author: Jessime

Description
-----------

"""

import gzip
import pickle
import louvain
import networkit
import champ
import numpy as np
import igraph as ig
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt

from my_tqdm import my_tqdm

class WeightCollector:

    def __init__(self):
        self.weights = []

    def node_edge_weights(self, n1, n2, ew, e_id):
        self.weights.append(ew)

class GraphMaker2:
    """Similar functionality to GraphMaker, but uses `networkit` for speed

    Parameters
    ----------
    sim: coo_matrix
        Sparse adjacency matrix representation of weighted edges
    el_path: str
        Path to new gzip file for storing an edge list.
    node_threshold_map: {int:float}
        Keys are nodes. Values are the weight limits below which edges are filtered.
    map_path: str
        Path to new file for node_threshold_map

    Attributes
    ----------
    """
    def __init__(self, sim=None, el_path=None, node_threshold_map=None, map_path=None):
        self.sim = sim
        self.el_path = el_path
        self.node_threshold_map = node_threshold_map
        self.map_path = map_path

        self.graph = None
        self.main_sub = None
        self.partition = None

    def make(self, clear_mem=True):
        self.graph = networkit.graph.Graph(self.sim.shape[0], weighted=True)
        zipped = zip(self.sim.row, self.sim.col, self.sim.data)
        for i, j, w in my_tqdm()(zipped, total=self.sim.size):
            self.graph.addEdge(i, j, w)
        if clear_mem:
            self.sim = None #free memory

    def build_node_threshold_map(self):
        self.node_threshold_map = {}
        for n in my_tqdm()(self.graph.nodes()):
            weight_collector = WeightCollector()
            self.graph.forEdgesOf(n, weight_collector.node_edge_weights)
            weights = np.sort(np.array(weight_collector.weights))
            limit_idx = -101 if len(weights) > 101 else 0
            self.node_threshold_map[n] = weights[limit_idx]
        if self.map_path is not None:
            pickle.dump(self.node_threshold_map, open(self.map_path, 'wb'))

    def make_with_node_threshold(self, clear_mem=True):
        #NOTE Keep separate from `make` for speed
        self.graph = networkit.graph.Graph(self.sim.shape[0], weighted=True)
        zipped = zip(self.sim.row, self.sim.col, self.sim.data)
        last_i = None #So we don't have to lookup i everytime
        last_i_limit = None
        for i, j, w in my_tqdm()(zipped, total=self.sim.size):
            if i != last_i:
                i_limit = self.node_threshold_map[i]
                last_i = i
            j_limit = self.node_threshold_map[j]
            limit = min(i_limit, j_limit)
            if w >= limit:
                self.graph.addEdge(i, j, w)
        if clear_mem:
            self.sim = None #free memory

    def make_threshold_remake(self):
        """Build graph from sparse matrix, calculate thresholds, remake graph with thresholds.

        Notes
        -----
        This only has to be run once per graph if you save the node_threshold_map.
        Then you can skip straight to `make_with_node_threshold`
        """
        self.make(clear_mem=False)
        self.build_node_threshold_map()
        self.make_with_node_threshold()

    def find_main_sub(self):
        dcc = networkit.components.DynConnectedComponents(self.graph)
        dcc.run()
        self.main_sub = self.graph.subgraphFromNodes(dcc.getComponents()[0])

    def get_partition(self, main_sub=True, gamma=1, max_comm_n=None):
        """Calculate louvain

        Parameters
        ----------
        main_sub: bool (default=True)
            Set to False to run Louvain on self.graph instead of self.main_sub
        max_comm_n: None, int (default=None)
            Maximum number of communities. Smaller communities are grouped into an additional single community.

        Return
        ------
        labels: [int]
            List of community labels found by Louvain
        """
        graph = self.main_sub if main_sub else self.graph
        algo = networkit.community.PLM(graph, refine=False, gamma=gamma)
        self.partition = networkit.community.detectCommunities(graph, algo)
        if max_comm_n is not None:
            size_map = self.partition.subsetSizeMap()
            size_sorted = sorted(size_map.items(), key=lambda x: x[1], reverse=True)
            map_by_size = {size_sorted[i][0]:i for i in range(max_comm_n)}
            labels = self.partition.getVector()
            labels = [map_by_size[x] if x in map_by_size else max_comm_n for x in labels]
        else:
            labels = self.partition.getVector()
        return labels

    def drop_duplicates(self):
        df = pd.read_csv(self.el_path, sep='\t', header=None, compression='gzip')
        df.drop_duplicates(inplace=True)
        df.to_csv(self.el_path, index=False, header=False, sep='\t',  float_format='%.5f')

    def save_edgelist(self, drop_duplicates=False):
        """Save a tab-delimited, gzipped file of edges and weights.

        Parameters
        ----------
        drop_duplicates : bool (default=False)
            If True, remove duplicate edges from list
        """
        if self.el_path is not None:
            with gzip.open(self.el_path, 'w') as outfile:
                def write(u, v, edgeweight, edgeid):
                    if u != v:
                        outfile.write(f'{u}\t{v}\t{edgeweight:.5f}\n')
                self.graph.forEdges(write)
        if drop_duplicates:
            self.drop_duplicates()

    def edgelist2igraph(self):
        """Convert an edgelist to a igraph Graph

        Returns
        -------
        graph : igraph.Graph
            igraph object representation of network. Can be written to gml.
        """
        df = pd.read_csv(self.el_path, compression='gzip', sep='\t', header=None)
        edges = list(zip(df[0].values.tolist(), df[1].values.tolist()))
        graph = ig.Graph(edges, edge_attrs={'weight': df[2].values.tolist()})
        return graph


class ChampRunner:
    """Help decide what resolution to use when defining communities.

    Parameters
    ----------
    el_path: str
        Path to edgelist file produced by GraphMaker
    fig_path: str
        Path to use for saving image of modularity domains
    df_path: str
        Path to use for saving best community assignments found by champ
    top_path: str
        Path to use for saving largest communities at each gamma
    ratios_path:
        Path to use for saving ratios between sizes of communities for each gamma.
        Useful for finding a gamma that has distinct size differences in communities.
    """
    def __init__(self, el_path=None, fig_path=None, df_path=None, top_path=None, 
            ratios_path=None, leiden=False):
        self.el_path = el_path
        self.fig_path = fig_path
        self.df_path = df_path
        self.top_path = top_path
        self.ratios_path = ratios_path
        self.leiden = leiden

        self.graph = None
        if el_path is not None:
            gm = GraphMaker2(el_path=el_path)
            self.graph = gm.edgelist2igraph()
        self.partition_ensemble = None
        self.df = None
        self.top_communities = None
        self.ratios = None

    def run_louvain(self):
        """Create a set of possible communities.

        Note: This is expensive in both memory and time.
        """
        self.partition_ensemble = champ.parallel_louvain(self.graph,
            start=.1, fin=5.1, numruns=100, weight='weight', progress=10, leiden=self.leiden)

    def to_df(self):
        """Create a df of community labels. Column names are gamma values."""
        df = np.array(self.partition_ensemble.get_CHAMP_partitions())
        gammas = self.partition_ensemble.get_champ_gammas()
        columns = [round(g, 3) for g in gammas[1:]]
        self.df = pd.DataFrame(data=df.T, columns=columns)
        if self.df_path is not None:
            self.df.to_csv(self.df_path)

    def plot(self):
        """Save resolution vs. modularity"""
        champ.plot_single_layer_modularity_domains(self.partition_ensemble.ind2doms, labels=True)
        if self.fig_path is not None:
            plt.savefig(self.fig_path, bbox_inches='tight', dpi=600)

    def find_top_communities(self):
        """Save the sizes of the largest 20 communities at each gamma.

        Note: The index is reset here, so the communities don't have the sample labels they did.
        """
        top_communities = []
        for i, col in self.df.items():
            top = col.value_counts().head(20).reset_index(drop=True)
            top_communities.append(top)
        self.top_communities = pd.concat(top_communities, axis=1, sort=False)
        if self.top_path is not None:
            self.top_communities.to_csv(self.top_path)

    def calc_ratios(self):
        """Calculate how many times larger each community is than the next largest community."""
        ratios = []
        index = []
        idx_flag = True
        for i, col in self.top_communities.items():
            ratios.append([])
            prev_j = 0
            prev_row = col[0]
            for j, row in col.iloc[1:].iteritems():
                ratios[-1].append(prev_row/row)
                if idx_flag:
                    index.append(f'{prev_j}-{j}')
                prev_j = j
                prev_row = row
            idx_flag = False
        self.ratios = pd.DataFrame(data=ratios, index=self.top_communities.columns, columns=index, dtype=int).T
        if self.ratios_path is not None:
            self.ratios.to_csv(self.ratios_path)

    def run(self):
        self.run_louvain()
        self.to_df()
        self.plot()
        self.find_top_communities()
        self.calc_ratios()


class Stability:
    """Find nodes that are consistantly in the top communities

    Parameters
    ----------
    gml_path: str
        Path to igraph .gml file
    out_path: str
        Path to igraph .gml file with `stable` attribute
    gamma: float
        Gamma chosen by CHAMP to use for network
    n_groups: int
        Number of non-null groups expected in graph

    Attributes
    ----------
    graph: igraph.Graph
    
    """
    def __init__(self, gml_path=None, out_path=None, gamma=1., n_groups=10):
        self.gml_path = gml_path
        self.out_path = out_path
        self.gamma = gamma
        self.n_groups = n_groups
        
        if self.gml_path is not None:
            self.graph = ig.Graph.Read_GML(self.gml_path)
        self.partition_ensemble = None
        self.group_df = None
        
    def run_louvain(self):
        start = self.gamma - .0001
        end = self.gamma + .0001
        self.partition_ensemble = champ.parallel_louvain(self.graph,
            start=start, fin=end, numruns=100, weight='weight', progress=10)
        
    def make_group_df(self):
        data = np.array(self.partition_ensemble.partitions).T
        columns = self.partition_ensemble.resolutions
        self.group_df = pd.DataFrame(data, columns= columns)

    def calc_stability(self):
        def percentage(row, n):
            #Use `<` here instead of `<=`, since groups are 0 based
            return sum(row < n) / len(row)
        self.group_df['stability'] = self.group_df.apply(percentage, 
                                                         axis=1, 
                                                         args=(self.n_groups,))
        self.group_df['stable'] = self.group_df['stability'] >= .9
        return self.group_df['stable'].value_counts()
        
    def find_mode_group(self):
        mode = self.group_df.mode(axis=1)[0]
        mode_vc = mode.value_counts()
        mapping_dict = {}
        for i, old_group in enumerate(mode_vc.index):
            if i < self.n_groups:
                mapping_dict[old_group] = i
            else:
                mapping_dict[old_group] = self.n_groups
        self.group_df['group'] = mode.map(mapping_dict)
        
    def add_to_graph(self):
        self.graph.vs['stable'] = self.group_df['stable']
        self.graph.vs['group'] = self.group_df['group']
        if self.out_path is not None:
            self.graph.write_gml(self.out_path)
            
    def run(self):
        self.run_louvain()
        self.make_group_df()
        self.calc_stability()
        self.find_mode_group()
        self.add_to_graph()
        


class GraphMaker:
    """Class for generating and manipulating network graphs

    Parameters
    ----------
    dist : str or DataFrame
        (Path to) matrix of distance measurements
    """
    def __init__(self, dist=None, gmlfile=None, limit=0, gamma=1):
        self.dist = dist
        if isinstance(self.dist, str):
            self.dist = pd.read_csv(self.dist, index_col=0)
        self.gmlfile = gmlfile
        self.limit = limit
        self.gamma = gamma

        self.graph = None
        self.main_sub = None
        self.partition = None
        self.node_colors = 'r'

    def threshold(self):
        np.fill_diagonal(self.dist.values, self.limit) #TODO: Why am I doing this?
        self.dist.values[self.dist.values < self.limit] = 0

    def find_main_sub(self):
        if self.graph is None:
            self.make()
        subgraphs = list(nx.connected_component_subgraphs(self.graph))
        graph_sizes = [sub.size() for sub in subgraphs]
        self.main_sub = subgraphs[graph_sizes.index(max(graph_sizes))]

    def get_partition(self, infile=None):
        if infile is None:
            infile = self.gmlfile
        graph = ig.Graph.Read_GML(infile)
        print('Finished reading file')
        self.partition = louvain.find_partition(graph,
                                                louvain.RBConfigurationVertexPartition,
                                                weights='weight',
                                                resolution_parameter=self.gamma)

    def membership2attribute(self, lim=1E9):
        """Run louvain and store communities in graph"""
        if self.partition is None:
            print('Running partition')
            self.get_partition()
        print('Finished partitioning. Creating attribute.')
        zipped = zip(self.graph.nodes(), self.partition.membership)
        name2group = {k:v if v <= lim-1 else lim for k, v in zipped}
        nx.set_node_attributes(self.graph, name='Group', values=name2group)

    def fold_change(self, lim=7):
        sizes = self.partition.sizes()[:lim]
        fold_change = [sizes[i]/sizes[i+1] for i in range(len(sizes)-1)]
        for i, fc in enumerate(fold_change[:7]):
            print(f'{i}-{i+1}: {fc:.2f}')

    def partition_summary(self, fc_lim=7):
        print('-------')
        print('Summary')
        print('-------')
        print('')
        print("Quality: ", self.partition.quality())
        print("Modularity: ", self.partition.modularity)
        print("Group #: ", self.partition._len)
        print("Sizes :", self.partition.sizes()[:15])
        print('')
        self.fold_change(fc_lim)

    def draw(self):
        print('Drawing...')
        nx.drawing.draw_spring(self.graph,
                       node_size=50,
                       node_color=self.node_colors,
                       font_size=14,
                       alpha=.5,
                       edge_color='lightgrey',
                       cmap='Set1')

    def group2colors(self):
        #TODO check if nx.get_node_attributes(self.graph, 'Group').values() is sufficient
        group = nx.get_node_attributes(self.graph, 'Group')
        self.node_colors = [group[n] for n in self.graph.nodes()]

    def add_node_labels(self):
        """Add node labels to graph from DataFrame"""
        name_map = dict(zip(self.graph.nodes(), self.dist.index))
        self.graph = nx.relabel.relabel_nodes(self.graph, name_map)

    def make(self):
        self.threshold()
        self.graph = nx.from_numpy_matrix(self.dist.values)
        self.add_node_labels()
        self.dist = None #Clear some memory

    def save(self, main_sub=False):
        """Saves graph to gmlfile in the .gml format"""
        graph = self.main_sub if main_sub else self.graph
        nx.write_gml(graph, self.gmlfile)

