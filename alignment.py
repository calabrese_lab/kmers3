# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 16:54:43 2015

@author: Jessime

Description
-----------
Tools for running aligners in with Python

I probably want to have one of two things instead of completely separate alignment classes:
    1. A parent class
    2. A single class where you choose your alignment algorithm
"""

import os
import shutil
import numpy as np
import pandas as pd
import subprocess as sp

from itertools import product
from Bio.Emboss.Applications import StretcherCommandline, WaterCommandline

from fasta import Maker
from my_tqdm import my_tqdm

class Emboss:
    """Runs the BioPython stretcher application and handles output.

    Parameters
    ----------
    aligner : str (default='stretcher')
        Pass 'stretcher' to perform global alignment, and 'water' for local alignment.
    infasta1 : str (default=None)
        Path to the first fasta file to be aligned
    infasta2 : str (default=None)
        Path to the second fasta file to be aligned
    outfile : str (default=None)
        Path to the output file from a stretcher run
    """
    def __init__(self, aligner_name='stretcher', infasta1=None, infasta2=None, outfile=None):
        self.aligner_name = aligner_name
        aligner_dict = {'stretcher': StretcherCommandline,
                        'water': WaterCommandline}
        try:
            self.aligner = aligner_dicanalyze_t[aligner_name]
        except KeyError:
            print('aligner must be "stretcher" or "water".')
            raise

        self.infasta1 = infasta1
        self.infasta2 = infasta2
        self.outfile = outfile

        self.maker1 = None
        self.maker2 = None
        self.temp_dir = None
        self.temp_dir1 = None
        self.temp_dir2 = None
        self.similarity_df = None

    def align(self):
        """Runs aligner and saves to self.outfile"""
        cline = self.aligner()
        cline.asequence = self.infasta1
        cline.bsequence = self.infasta2
        if isinstance(cline, WaterCommandline):
            cline.gapopen = 10
            cline.gapextend = .5
        else:
            cline.gapopen = 16
            cline.gapextend = 4
        cline.outfile = self.outfile
        stout, sterr = cline()

    def parse_results(self):
        """Reads self.outfile and finds the similarity from the 25 line and score from line 27.

        Returns
        -------
        similarity : float
            Similarity of the two sequences as a decimal
        score : float
            Total score of the alignment
        """
        with open(self.outfile) as strecher_file:
            for i in range(24):
                next(strecher_file)
            similarity = next(strecher_file).split()[2].split('/')
            similarity = float(similarity[0])/float(similarity[1])
            next(strecher_file)
            score = float(next(strecher_file).split()[2])
        return similarity, score

    def build_files(self):
        """Separates fasta files."""
        self.temp_dir = os.path.join(os.path.dirname(self.infasta1), 'strecher_temp')
        self.temp_dir1 = os.path.join(self.temp_dir, '1/')
        self.temp_dir2 = os.path.join(self.temp_dir, '2/')

        os.makedirs(self.temp_dir1)
        os.makedirs(self.temp_dir2)
        self.maker1 = Maker(self.infasta1)
        self.maker2 = Maker(self.infasta2)
        self.maker1.separate(self.temp_dir1)
        self.maker2.separate(self.temp_dir2)

    def remove_files(self):
        """Remove everything created by build_files"""
        shutil.rmtree(self.temp_dir, ignore_errors=True)

    def align_loop(self, save=True):
        """Does all possible pairwise alignments if fasta files have more than one sequence.

        Parameters
        ----------
        save : bool (default=True)
            Set to False to only output information and not create a DataFrame

        Notes
        -----
        It would be nice to have an intelligent way to handle all sequences existing in one fasta file.
        Right now the solution is to pass the same .fa twice.
        This doubles the runtime.
        But we also want to have a way to run two separate files;
        so this function fulfills those requirements.

        Check outputs for progress (not using my_tqdm)
        """
        self.build_files()
        if save:
            score_ls = []
        for i, j in product(range(len(self.maker1.data)), range(len(self.maker2.data))):
            current_infasta1 = os.path.join(self.temp_dir1, '{}.fa'.format(i))
            current_infasta2 = os.path.join(self.temp_dir2, '{}.fa'.format(j))
            current_outfile = os.path.join(self.temp_dir, '{}vs{}.txt'.format(i, j))
            current_emboss = Emboss(self.aligner_name,
                                    current_infasta1,
                                    current_infasta2,
                                    current_outfile)
            current_emboss.align()
            if save:
                similarity, score = current_emboss.parse_results()
                score_ls.append([i, j, similarity, score])
        if save:
            columns = ['i', 'j', 'similarity', 'score']
            self.similarity_df = pd.DataFrame(data=score_ls, columns=columns)
            self.remove_files()

class Nhmmer:

    def __init__(self, infasta1=None, infasta2=None, outdir=None, handle_empty_table=False):
        self.infasta1 = infasta1
        self.infasta2 = infasta2
        self.outdir = outdir
        self.handle_empty_table = handle_empty_table

        self.fastas_dir = None
        self.tables_dir = None
        self.sim = None
        self.names = None

    def split_infasta1(self):
        """Split the query fasta file into single sequences per file"""
        self.fastas_dir = os.path.join(self.outdir, 'fastas')
        try:
            os.makedirs(self.fastas_dir)
            self.maker = Maker(self.infasta1)
            self.maker.separate(self.fastas_dir)
        except FileExistsError:
            print('Fasta folder already created')

    def align(self, i):
        query = os.path.join(self.fastas_dir, f'{i}.fa')
        outfile = os.path.join(self.tables_dir, f'{i}.tbl')
        cmd = f'nhmmer --nonull2 --nobias --noali --tblout {outfile} {query} {self.infasta2}'
        sp.run(cmd.split())

    def make_tables(self, start=0, end=None):
        """Run an nhmmer alignment for each transcript

        Parameters
        ----------
        start : int (default=0)
            First transcript index to align. Useful if run was interupted.
        """
        self.split_infasta1()
        self.tables_dir = os.path.join(self.outdir, 'tables')
        try:
            os.makedirs(self.tables_dir)
        except FileExistsError:
            print('Table folder already created')

        if end is None:
            end = len(os.listdir(self.fastas_dir))
        for i in range(start, end):
            self.align(i)

    def read_table(self, table):
        try:
            table = pd.read_csv(table, 
                                engine='python', 
                                skiprows=2, 
                                skipfooter=10, 
                                header=None, 
                                sep='\s+')
        except pd.errors.EmptyDataError:
            if self.handle_empty_table:
                return None
            raise pd.errors.EmptyDataError(f'{table} is empty and `handle_empty_table` is False.')
        return table

    def process_table(self, i, gencode, threshold=.05):
        table = os.path.join(self.tables_dir, f'{i}.tbl')
        table = self.read_table(table)
        if table is None:
            row = np.zeros(len(self.names))
            row[i] = 1
            return row
        table = table.rename(columns={0:'target name', 12:'E-value', 13:'score'})
        table = table[table['E-value'] <= threshold].copy()

        name_col = 'target name'
        if gencode:
            table['common_name'] = table[name_col].apply(lambda x: x.split('|')[4])
            name_col = 'common_name'
        table['norm_score'] = table['score'] / table.iloc[0]['score']
        table_max = table.sort_values('norm_score', ascending=False).drop_duplicates(name_col)
        name_score = dict(zip(table_max[name_col], table_max['norm_score']))
        row = [name_score[n] if n in name_score else 0 for n in self.names]
        return np.array(row)

    def tables2sim(self, names=None, gencode=True, threshold=.05):
        """Create a similarity matrix from the results of make_tables

        Parameters
        ----------
        names : str | [str] (default=None)
            (Path to) common names for sequences. If None, 'target name' column is used.
        gencode : bool (default=True)
            If True, extract the 'common' name from the target_name column, otherwise use whole name.
        """
        n = len(os.listdir(self.tables_dir))
        self.sim = np.zeros((n, n))

        if isinstance(names, str):
            names = pickle.load(open(self.names, 'rb'))
        self.names = names

        for i, row in enumerate(my_tqdm()(self.sim)):
            self.sim[i] = self.process_table(i, gencode, threshold)
