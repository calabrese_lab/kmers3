"""This script handles downloading and processing all ENCODE total RNAseq experiments.

The basic algorithm is:

1. Load a set of RNAseq experiments I want to process
2. For each experiment
    A. Find the fastq files
    B. Decide if the experiment is paired end or not
    C. Make a folder
    D. Download the fastq files, keeping track of paired ends
    E. Run an appropriate STAR command using on the set of fastq files
    F. Run salmon on the STAR output
(3) Build matrix of TPM counts in all cell types
(4) Analyze matrix to find correlations

3 and 4 are handled by another script.

At this point, I'm only going to have to run this script once.
So some small portions have been hardcoded.
For the most part, the hardcoded parts have been religated to star_salmon.sh

Parameters
----------
url: link to the set of ENCODE experiments to be processed
outdir: The directory to store all processed
"""

import os
import sys

import encode

from multiprocessing import Pool
from itertools import cycle

def run_STAR_salmon(workdir, infiles):
    cmd = f'./star_salmon.sh {workdir} {infiles}'
    sp.run(cmd.split())

def make_fastq_name(json):
    """Creates an appropriate file name for both single and paired end fastq files"""
    name = json['accession']
    if 'paired_with' in json:
        rep = json['replicate']['biological_replicate_number']
        pair = json['paired_end']
        outname = f"P{pair}_R{rep}_{name}.fastq.gz"
    else:
        outname = f'{name}.fastq.gz'
    return outname

def download_fastq(json, expdir):
    """Downloads a single fastq file to and from appropriate locations"""
    outname = make_fastq_name(json)
    url = f"https://www.encodeproject.org{json['href']}"
    full_outpath = os.path.join(expdir, outname)
    encode.Experiment.stream_file(url, full_outpath)
    return outname

def make_star_file_string(files):
    """Creates a string to be passed to STAR's --readFilesIn parameter"""
    files.sort()
    paired = files[0][0] == 'P'
    if paired:
        p1 = ','.join(files[:len(files)//2])
        p2 = ','.join(files[len(files)//2:])
        string = f'{p1} {p2}'
    else:
        string = ','.join(files)
    return string

def process_experiment(exp_id, outdir):
    """Finds, downloads, and processes all fastq files in experiment"""
    print(f'Starting {exp_id}.')
    exp = encode.Experiment(exp_id)
    expdir = os.path.join(outdir, exp_id)
    os.makedirs(expdir)
    #print(f'Made: {expdir}.')

    fastq_ls = exp.filter_filetype({'file_format': 'fastq'})
    if not fastq_ls: #A couple of experiments are empty
        print(f'Error: {exp_id} is empty.')
        return

    fq_names = [download_fastq(f, expdir) for f in fastq_ls]
    star_files = make_star_file_string(fq_names)
    run_STAR_salmon(expdir, star_files)
    print(f'Done with {exp_id}!!')

def main(url, outdir):
    """Processes each experiment found in json data returned by url"""
    search = encode.Search(url)
    search.run()
    n = 50
    data = zip(search.gen_experiment_uids(), cycle([outdir]))
    with Pool(n) as p:
        p.starmap(process_experiment, data)

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
