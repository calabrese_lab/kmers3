# -*- coding: utf-8 -*-

import time
import smtplib
# import notify

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
#from twilio.rest import Client
from email.mime.text import MIMEText
from traceback import format_exc

class Scraper():

    def __init__(self):
        self.driver = webdriver.Firefox()
        self.driver.get("https://cts.sciencemag.org/scc/")

        self.sci_user = 'jessime@email.unc.edu'
        self.sci_pass = 'Scienceiscool1'
        self.gmail_user = 'jessime.kirk@gmail.com'
        self.gmail_pass = 'what is the problem'
        self.email_list = ['mauro_calabrese@med.unc.edu',
                           'sokim@live.unc.edu',
                           'davidlee@email.unc.edu',
                           'schertze@email.unc.edu',
                           'jessime@email.unc.edu']
        self.current_status = 'Under Evaluation'

        username_element = self.driver.find_element_by_id("email")
        password_element = self.driver.find_element_by_id("password")
        username_element.send_keys(self.sci_user)
        password_element.send_keys(self.sci_pass)
        self.driver.find_element_by_id("login-button").click()

    # def text_new_status(self, status):
    #     accountSID = 'AC93ec96cbd340e65a9c75b39622b3c56e'
    #     authToken = '7fbd7c232664a2155b9bd4c5a950d08d'
    #     twilioCli = Client(accountSID, authToken)
    #     twilioCli.messages.create(body=status,
    #                               from_='3302952091',
    #                               to='6067485863')

    def email_new_status(self, message):
        try:
            server = smtplib.SMTP(host='smtp.gmail.com', port=587)
            server.ehlo()
            server.starttls()
            server.login(self.gmail_user, self.gmail_pass)
            for recipient in self.email_list:
                msg = MIMEText(message)
                msg['Subject'] = 'Updated status for Science paper.'
                msg['From'] = self.gmail_user
                msg['To'] = recipient
                server.send_message(msg)
                print(f'\nEmail sent to: {recipient}')
            server.quit()
        except Exception:
            error = format_exc()
            print(error)

    def update(self):
        self.driver.refresh()
        table = self.driver.find_element_by_id("my-tasks-grid")
        time.sleep(5)
        row = table.find_elements_by_tag_name("tr")[1]
        col = row.find_elements_by_tag_name('td')[6].text

        if col == self.current_status:
            print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime()))
        else:
            print(f'HEY, HEY, HEY!!! Check the new status: {col}')
            message = f'Our status has moved from {self.current_status} to {col}. \n\n Cheers!'
            self.email_new_status(message)
            self.current_status = col

        time.sleep(600)


# @notify.text_error
def run():
    scraper = Scraper()
    time.sleep(5)
    while True:
        scraper.update()

if __name__ == '__main__':
    run()
