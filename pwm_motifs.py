import numpy as np
import pandas as pd
import pickle

from collections import defaultdict
from itertools import product
from IPython.display import display

class MotifWeighter:

    def __init__(self, pwm_ls, group_kmers, protein, comms, outweights, outgroups, outsums):
        self.pwm_ls = pwm_ls
        self.group_kmers = pd.read_csv(group_kmers, index_col=0).T
        self.protein = protein
        self.comms = pickle.load(open(comms, 'rb'))
        self.outweights = outweights
        self.outgroups = outgroups
        self.outsums = outsums

        self.weight_mean = None
        self.group_weighted = None
        self.group_sums = None

    def kmers_at_start_pos(self, pos, pwm, kmer_weights):
        for kmer in (''.join(i) for i in product('AGTC', repeat=6)):
            weights = [pwm.get_value(i+pos, nuc) for i, nuc in enumerate(kmer)]
            weight = np.prod(np.array(weights))
            kmer_weights[kmer] += weight
        return kmer_weights

    def pwm2df(self, pwm):
        pwm = pd.read_csv(pwm, index_col=0, sep='\t', skiprows=6)
        pwm.columns = list('ACGT')
        kmer_weights = defaultdict(float)
        for pos in range(1, max(pwm.index)-4):
            kmer_weights = self.kmers_at_start_pos(pos, pwm, kmer_weights)
        kmer_weights_df = pd.DataFrame.from_dict(kmer_weights, orient='index')
        kmer_weights_df.columns = ['weight']
        return kmer_weights_df

    def enriched_vs_un(self):
        sums = self.group_weighted.sum()
        comms = sums.index.isin(self.comms[self.protein])
        data = [[np.mean(sums[comms]), np.mean(sums[~comms])]]
        return pd.DataFrame(data, [self.protein], ['enriched', 'unenriched'])

    def pwm2weighted_kmers(self):
        self.weight_mean = sum(self.pwm2df(pwm) for pwm in self.pwm_ls)/len(self.pwm_ls)
        self.weight_mean.to_csv(self.outweights)
        self.group_weighted = self.group_kmers.copy()
        self.group_weighted = self.group_weighted.mul(self.weight_mean['weight'], axis='index')
        self.group_weighted.to_csv(self.outgroups)
        self.enriched = self.enriched_vs_un()
        self.enriched.to_csv(self.outsums)
