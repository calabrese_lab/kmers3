#!/usr/bin/env python

import sys
import os
import subprocess as sp

def process_experiment(expdir):
    try:
        with open(os.path.join(expdir, 'star_str.txt')) as infile:
            star_str = infile.read()
    except FileNotFoundError:
        print(f'Error: Skipping {expdir} because star_str.txt is missing.')
        return
    cmd = f'sbatch star_salmon.sh {expdir} {star_str}'
    sp.run(cmd.split())

def main(workdir):
    for f in os.listdir(workdir):
        expdir = os.path.join(workdir, f)
        process_experiment(expdir)
        print(expdir)

if __name__ == '__main__':
    main(sys.argv[1])
