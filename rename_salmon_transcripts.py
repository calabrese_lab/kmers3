#!/usr/bin/env python

import sys
import argparse
import pandas as pd

import gtf

def dq2ercc(df):
    ercc_gtf = '/proj/calabrlb/users/Jessime/data/ERCC92.gtf'
    tx_name2ercc_id = {}
    with open(ercc_gtf) as ercc_gtf:
        for line in ercc_gtf:
            data = line.strip().split()
            tx_name2ercc_id[data[-1][1:-2]] = data[0]
    df = df.replace({'Name':tx_name2ercc_id})
    return df

def rename(in_gtf, in_salmon, outfile):
    print('Reading GENCODE.')
    quant_df = pd.read_csv(in_salmon, sep='\t')
    quant_df = dq2ercc(quant_df)

    maker = gtf.Maker(in_gtf)
    maker.filter_feature()
    print('Parsing ID.')
    maker.append_attribute(col=1, name='transcript_id')
    print('Parsing name')
    maker.append_attribute(col=5, name='transcript_name')

    names_df = maker.expandedDF[['transcript_id', 'transcript_name']]
    names_df = names_df.set_index('transcript_id')

    print('Joining tables.')
    quant_common = quant_df.set_index('Name')
    quant_common = quant_common.join(names_df)
    func = lambda row: row.name if 'ERCC' in row.name else row.transcript_name
    quant_common['transcript_name'] = quant_common.apply(func, axis=1)

    print('Saving and finishing.')
    quant_common.to_csv(outfile, sep='\t')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(usage=__doc__,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('in_gtf', help='Path to GENCODE .gtf file')
    parser.add_argument('in_salmon', help='Path to quant.sf from salmon output')
    parser.add_argument('outfile', help='Path to new file with common names appended')
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(0)
    args = parser.parse_args()
    rename(args.in_gtf, args.in_salmon, args.outfile)
