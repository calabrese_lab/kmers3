from sys import argv
from RNAtools import dotPlot

def main(infile, outfile):
    data = dotPlot(infile)
    shan = data.calcShannon(toFile=outfile)

if __name__ == '__main__':
    main(argv[1], argv[2])
