##########################################

# http://bioconductor.org/packages/release/bioc/vignettes/tximport/inst/doc/tximport.html
# http://bioconductor.org/packages/devel/bioc/vignettes/DESeq2/inst/doc/DESeq2.html

##########################################

library("tximport")
library("DESeq2")

args <- commandArgs(trailingOnly = TRUE)
#args <- c('~/Desktop/salmon_test/', '~/Research/v26_full_unspliced_tx2gene.csv')
encode.paths <- file.path(args[1], list.files(args[1]), 'salmon', 'quant.sf')
#encode.paths <- file.path(args[1], list.files(args[1]), 'quant.sf')
print(encode.paths)

tx2gene <- read.csv(args[2])
txi <- tximport(encode.paths, type="salmon", tx2gene=tx2gene)
write.csv(txi$counts, file=file.path(args[1], '../salmon_raw_counts.csv'))
blank_df <- DataFrame(c(1:ncol(txi$counts)))
ddsTxi <- DESeqDataSetFromTximport(txi, blank_df, ~1)
dds <- DESeq(ddsTxi)
print('transforming')
vsd <- varianceStabilizingTransformation(dds, blind=FALSE)
print('saving counts')
write.csv(assay(vsd), file=file.path(args[1], '../salmon_norm_counts.csv'))
