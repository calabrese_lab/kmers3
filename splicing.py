import pandas as pd

import gtf

class UnsplicedMaker(gtf.Maker):
    """Insert unspliced annotations into a GTF file

    Parameters
    ----------
    inGTF: str (default=None)
        Path to input GTF file
    outGTF: str (default=None)
        Path to output GTF file
    skiprows: int (default=None)
        Number of rows to skip in inGTF file
    label_start: int (default=0)
        Initial number used to label UNSPLICED transcripts.

    Attributes
    ----------
    new_gtf:
        Iteratively built DataFrame of new GTF file
    """
    def __init__(self, inGTF=None, outGTF=None, skiprows=None, label_start=0):
        super().__init__(inGTF=inGTF, outGTF=outGTF, skiprows=skiprows)
        self.n = n

        self.new_gtf = None

    def gene_id(self, row):
        return row.attribute.split(';')[0].split()[1].strip('\"')

    def compare_genes(self, row, next_row):
        """Compare is two rows have the same gene feature"""
        row_id = self.gene_id(row)
        next_id = self.gene_id(next_row)
        return row_id == next_id

    def check_unspliced(self, i, row):
        """Search for an unspliced transcript already included in the GTF file.

        Parameters
        ----------
        i: int
            index location in original GTF DataFrame
        row: Series
            current row, containing a gene feature

        Returns
        -------
        unspliced_included: bool
            True if the original GTF file already has an unspliced annotation for this gene
        """
        past = 0
        unspliced_included = False

        while True:
            past += 1
            if (i+past) not in self.inGTF.index:
                break
            next_row = self.inGTF.iloc[i+past]
            same_genes = self.compare_genes(row, next_row)
            if not same_genes:
                break
            conditions = (next_row['feature'] == 'exon',
                          next_row['start'] == row['start'],
                          next_row['end'] == row['end']
                          )
            if all(conditions):
                unspliced_included = True
                break

        return unspliced_included

    def building_base(self, row):
        attr = row['attribute'].split('; ')
        new = [attr[0]]
        new.append(f'transcript_id "UNSPLICED{self.label_start}"')
        new.append(attr[1])
        new.append(attr[2])
        new.append('transcript_type "unspliced"')
        name = attr[2].split()[1].strip('\"') + '-un'
        new.append(f'transcript_name "{name}"')
        return attr, new

    def build_transcript_attr(self, row):
        self.label_start += 1
        attr, new = self.building_base(row)
        new.append(attr[3])
        new.append('transcript_support_level "N/A"')
        new = '; '.join(new)
        return new

    def build_exon_attr(self, row):
        attr, new = self.building_base(row)
        new.append('exon_number 0')
        new.append(f'exon_id "EXON{self.label_start}"')
        new = '; '.join(new)
        return new

    def insert_unspliced(self, row):
        new_transcript = row.copy()
        new_transcript['feature'] = 'transcript'
        new_transcript['attribute'] = self.build_transcript_attr(row)
        self.new_gtf.append(new_transcript)

        new_exon = row.copy()
        new_exon['feature'] = 'exon'
        new_exon['attribute'] = self.build_exon_attr(row)
        self.new_gtf.append(new_exon)

    def run(self):
        self.new_gtf = []

        for i, row in self.inGTF.iterrows():
            self.new_gtf.append(row)
            if row['feature'] == 'gene':
                unspliced_included = self.check_unspliced(i, row)
                if not unspliced_included:
                    self.insert_unspliced(row)
        self.new_gtf = pd.DataFrame(data=self.new_gtf,
                                    index=range(len(self.new_gtf)))
        self.save(self.new_gtf)
