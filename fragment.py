# -*- coding: utf-8 -*-
"""
Created on Mon Jan  4 17:16:01 2016

@author: jessime

Description
-----------
Find the most similar lncRNA fragments to a lncRNA of interest.

Warnings
--------
Fragmenting a fasta file of any size is going to take a non-trivial amount of space.

Notes
-----
It's way faster to compute the fragments each time then to read/write them.
"""

from __future__ import print_function
from os import makedirs
from os.path import join, exists
from sys import stdout
from multiprocessing import Pool
from tqdm import tqdm
from itertools import izip

import argparse
import shutil
import numpy as np
import pandas as pd

import kmer_counts

from fasta import Extracter
from fasta_reader import Reader

def warn_exists(path):
    """Immediately warn that a which should be created is already made"""
    warn = 'WARNING! {} already exists. Continuing with previous path.'
    print(warn.format(path)); stdout.flush()


class Fragmenter(object):
    """

    Attributes
    ----------
    pwd : str
        The directory of the fasta files. This is where output files will be saved.
    database_fasta : str
        Name of the fasta file to be fragmented and searched.
    query_fasta : str
        Name of the fasta file containing the single fasta sequence of interest
    frag_size : int
        The length of the fragments to be created. Generally the same length as the query seqeunce.
    k : int
        The kmers size to count
    frag_file : str
        This file name is passed to the Maker class.
        Maker will automatically append numbers to this file name as it generates fragment chunk files.
    frag_names : str
        The name of the file containing the pickled names of each of the generated fragments
    count_file : str
        The name of the numpy file containing all the vertically stacked counts
    count_file_norm : str
        The name of the numpy file containing the row and normalized counts
    dist_file : str
        The name of the numpy file containing the Pearson matrix of all the fragments
    """

    def __init__(self, pwd, database_fasta, query_fasta, frag_size,
                 k=4, cores=4, suffix='', save_counts=False, verbose=True):
        self.database_fasta = join(pwd, database_fasta)
        self.query_fasta = join(pwd, query_fasta)
        self.data, self.names, self.seqs = self.get_data(self.database_fasta,
                                                        self.query_fasta)
        self.frag_size = frag_size
        self.k = k
        self.cores = cores #TODO get rid of this (make sure Pipeline still works after removal)
        self.save_counts = save_counts
        self.verbose = verbose

        self.mean = np.zeros(4**k)
        self.std_dev = np.zeros(4**k)
        self.n = 0

        self.query_counts = None
        self.seq_lengths = None

        self.frag_names_file = join(pwd, 'frag_names{}.txt'.format(suffix))
        self.count_file = join(pwd, '{}mers01{}.npy'.format(k, suffix))
        self.count_file_norm = join(pwd, '{}mers01_norm{}.npy'.format(k, suffix))
        self.dist_file = join(pwd, 'frag_dist{}.npy'.format(suffix))

    def get_data(self, database_fasta, query_fasta):
        """Process fasta data into a list of sequences"""
        data, names, seqs = Reader(database_fasta).get_data()
        q_data, q_names, q_seqs = Reader(query_fasta).get_data()
        assert q_names[0] not in names, 'The query RNA\'s common name must be unique.'
        data = data + q_data
        names = names + q_names
        seqs = seqs + q_seqs
        return data, names, seqs

    def find_seq_lengths(self):
        """This information needs to be extracted while seqs are in memory.
        It'll be passed to the Analyzer when it's time to make names"""
        self.seq_lengths = [len(seq) for seq in self.seqs]

    def online_moments(self, counts):
        """Update the mean and std. vectors"""
        for x in counts:
            self.n += 1
            delta = x - self.mean
            self.mean += delta/self.n
            self.std_dev += delta*(x - self.mean)

    def col_norm(self, counts):
        """Column normalize"""
        counts -= self.mean
        counts /= self.std_dev
        return counts

    def row_norm(self, counts):
        """Row normalize"""
        counts = (counts.T - np.mean(counts, axis=1)).T
        counts = (counts.T / np.std(counts, axis=1)).T
        return counts

    def calc_dist(self, counts):
        """Calculates the Pearson of the last row of a numpy array with all other rows
        """
        counts = self.row_norm(counts)
        dist = np.inner(self.query_counts, counts)/counts.shape[1]
        return dist

    def stream_counts(self, seq):
        """Pickle-able function farmed out to engines to make fragment counts"""
        frag = seq[:self.frag_size]
        length = float(len(frag))
        inc = 1000/length
        first_kmer = frag[:self.k]
        frag_num = max(1, len(seq)-self.frag_size+1)

        try:
            counts = np.zeros([frag_num, 4**self.k], dtype=np.float32)
        except MemoryError:
            print('A {}x{} matrix is too large'.format(frag_num, 4**self.k))
            raise
        counter = kmer_counts.BasicCounter(k=self.k,
                                           centered=False,
                                           standardized=False,
                                           silent=True)
        counter.seqs = [frag]
        counter.get_counts()
        counts[0] = np.squeeze(counter.counts)

        for i in xrange(1, counts.shape[0]):
            frag = seq[i:i+self.frag_size]
            last_kmer = frag[-self.k:]

            counts[i] = counts[i-1]
            if first_kmer != last_kmer:
                try:
                    minus_index = counter.map[first_kmer]
                    counts[i, minus_index] -= inc
                except KeyError:
                    pass
                try:
                    plus_index = counter.map[last_kmer]
                    counts[i, plus_index] += inc
                except KeyError:
                    pass
            first_kmer = frag[:self.k]

        return counts

    def seqs_progress(self):
        """Determine if sequences list should be wrapped in a progress bar"""
        if self.verbose:
            seqs = tqdm(self.seqs)
        else:
            seqs = self.seqs
        return seqs

    def find_norm_vectors(self):
        """Count for the first time to find the mean and std. dev. vectors"""
        query_seq = self.seqs.pop(-1)
        seqs = self.seqs_progress()

        for seq in seqs:
            counts = self.stream_counts(seq)
            self.online_moments(counts)

        self.query_counts = self.stream_counts(query_seq)
        self.online_moments(self.query_counts)
        self.std_dev /= self.n
        self.std_dev = np.sqrt(self.std_dev)

        self.query_counts = self.col_norm(self.query_counts)
        self.query_counts = self.row_norm(self.query_counts)

    def recount_and_dist(self):
        dist = np.zeros(self.n, dtype=np.float32)
        index = 0
        seqs = self.seqs_progress()

        for seq in seqs:
            counts = self.stream_counts(seq)
            counts = self.col_norm(counts)
            length = counts.shape[0]
            dist[index:index+length] = self.calc_dist(counts)
            index += length
        dist[-1] = self.calc_dist(self.query_counts)
        if self.verbose:
            print('DISTANCE SAMPLE')
            print(dist[-5:])
        np.save(self.dist_file, dist)

    def run(self):
        """Wrapper function for creating a normalized distance array of all
        fragments from a fasta file
        """
        self.find_seq_lengths()
        if self.verbose:
            print('Finding normalization vectors.')
        self.find_norm_vectors()
        if self.verbose:
            print('Making distance matrix')
        self.recount_and_dist()



class Analyzer(object):
    """Used to produces human readable data from the results of fragmenter.py

    Parameters
    ----------
    pwd : str
        The directory storing the resulting files from running fragmenter.py
    seq_lengths : [int]
        Length of each fragment; needed for lazy generation of names
    outDF : str (default=None)
        The name for saved DataFrame containing correlation information
    outfile : str (default=None)
        The name for the file containing all labeled fragment R values
    dist : array
        The R values for all of the fragments produced by fragmenter.py
    frag_names : array
        The names of all fragments. Loaded from frag_names.txt produced by fragmenter.py
    frag_names_dict : dict
        keys = indicies, values = list of fragment number and name

    Attributes
    ----------
    frag_df : DataFrame (default=None)
        Used to store final results as DataFrame if file location is not provided
    """

    def __init__(self, pwd, names, seq_lengths, frag_size, suffix='',
                 outDF=None, outfile=None, sd_cut=3., verbose=True):

        self.outDF = None if outDF is None else join(pwd, outDF)
        self.outfile = None if outfile is None else join(pwd, outfile)
        self.dist = np.load(join(pwd, 'frag_dist{}.npy').format(suffix))

        self.names = names
        self.seq_lengths = seq_lengths
        self.frag_size = frag_size

        self.sd_cut = sd_cut
        self.verbose = verbose

        self.r_cut = None
        self.frag_names_dict = None
        self.top_rnas = None
        self.frag_df = None

    def describe_std_dev(self):
        """Find the standard deviation of the R values"""
        mean = np.mean(self.dist)
        std = np.std(self.dist)
        self.r_cut = (self.sd_cut*std) + mean
        r_std = np.abs(self.dist-mean)/std
        return np.nditer(r_std)

    def gen_frag_names(self):
        extracter = Extracter()
        extracter.names = self.names
        try:
            common_names = extracter.get_names('common')
        except IndexError:
            common_names = self.names
            print('Warning: Failed to extract common names. Falling back to full name.')
        for name, length in zip(common_names, self.seq_lengths):
            if length <= self.frag_size:
                yield [np.nan, name]
            else:
                for i in xrange(length-self.frag_size+1):
                    yield [i, name]

    def find_top_rnas(self):
        """Find the R value for each RNA at the pos most related to main fragment

        Returns
        -------
        top_rnas : dict
            keys= RNA name, values= list of position, R value, Std. Dev
        """
        r_std = self.describe_std_dev()
        top_rnas = {}
        rows = izip(self.gen_frag_names(), np.nditer(self.dist), r_std)

        if self.verbose:
            loop = tqdm(rows)
        else:
            loop = rows

        for pos_name, r, sd in loop:
            pos = pos_name[0]
            name = pos_name[1]
            if name in top_rnas:
                if r > top_rnas[name][1]:
                    top_rnas[name] = [pos, r, sd]
            else:
                top_rnas[name] = [pos, r, sd]
        return top_rnas

    def save_DF(self):
        """Cast top_rnas as DataFrame and save.

        Notes
        -----
        Saves to file if outDF is provided. Otherwise df is saved as an attribute.
        """
        frag_df = pd.DataFrame(self.top_rnas).transpose()
        frag_df.columns = ['Pos', 'R value', 'Std. Dev.']
        frag_df = frag_df.sort_values('R value')
        if self.outDF is None:
            self.frag_df = frag_df
        else:
            frag_df.to_csv(self.outDF, sep='\t')

    def save_labeled_frags(self):
        """Dumps all fragments with their names and positions
        """
        if self.outfile is not None:
            with open(self.outfile, 'w') as outfile:
                for pos_name, r in izip(self.gen_frag_names(), np.nditer(self.dist)):
                    outfile.write('{}\t{}\n'.format(pos_name[1], r))

    def run(self):
        """Wrapper function to find the highest R value for each lncRNA,
        and save a corresponding DataFrame as well as labeled R values for all fragments.
        """
        if self.verbose:
            print('Running fragment analysis.')
        self.top_rnas = self.find_top_rnas()
        if self.verbose:
            print('Saving analysis files.')
        self.save_DF()
        self.save_labeled_frags()


def frag_and_analyze(data):
    """Run the Analyzer class for random RNAs and find the R threshold"""
    print('Starting fragmentation of random lncRNAs.'); stdout.flush()
    pwd = data[0]
    database_fasta = data[1]
    query_fasta = data[2]
    frag_size = data[3]
    k = data[4]
    cores = data[5]
    save_counts = data[6]
    outDF = data[7]
    outfile = data[8]
    sd_cut = data[9]
    verbose = data[10]

    fragger = Fragmenter(pwd,
                         database_fasta,
                         query_fasta,
                         frag_size,
                         k,
                         cores,
                         save_counts,
                         verbose=verbose)
    fragger.run()

    analyzer = Analyzer(pwd,
                        fragger.names,
                        fragger.seq_lengths,
                        frag_size,
                        sd_cut,
                        outDF=outDF,
                        outfile=outfile,
                        verbose=verbose)
    analyzer.run()
    r_cut = analyzer.r_cut
    return r_cut

class Pipeline(object):
    """Chains together all commands needed to find top RNA fragments.

    Pipeline expects three starting files in the same directory.
    Running the pipeline will complete the entire fragmenting process.
    It will find fragment counts for both the real and random databases,
    and compare the fragment distances to the query RNA.
    Using the random fragment database to draw an intelligent cutoff,
    the pipeline will save a DataFrame of most similar RNAs to the query.

    Attributes
    ----------
    pwd : str
        Directory of the fasta files. This is where output files will be saved.
    database_fasta : str
        Name of the fasta file to be fragmented and searched.
    query_fasta : str
        Name of the fasta file containing the single fasta sequence of interest
    frag_size : int
        Length of the fragments to be created. Generally the same length as the query seqeunce.
    outDF : str
        Name for saved DataFrame containing correlation information
    outfile : str
        Name for the file containing all labeled fragment R values
    k : int
        Kmers size to count
    sd_cut : int, float
        Number of std. devs. above the random average to draw the threshold
    cores : int
        Number of processes to use during the counting process
    rand_dir : str
        Path to directory storing all the files used for random fragments
    new_rand_q : str
        Path to copy of the query fasta file
    new_rand_db : str
        Path to relocated random database fasta file
    r_cut : float
        The R value corresponding to the threshold for most similar RNAs
    """
    def __init__(self, pwd, database_fasta, random_fasta, query_fasta,
                 frag_size, outDF, outfile, out_topDF=None, save_counts=False,
                 k=4, sd_cut=3., cores=4):
        self.pwd = pwd
        self.database_fasta = database_fasta
        self.random_fasta = random_fasta
        self.query_fasta = query_fasta
        self.frag_size = frag_size
        self.outDF = outDF
        self.out_topDF = out_topDF
        self.outfile = outfile

        self.save_counts = save_counts
        self.k = k
        self.sd_cut = sd_cut
        self.cores = cores

        self.rand_dir = join(pwd, 'random/')
        self.new_rand_q = None
        self.new_rand_db = None
        self.r_cut = None

    def setup_random(self):
        """Create the proper file structure to analyze random fragments"""
        if not exists(self.rand_dir):
            makedirs(self.rand_dir)
        else:
            warn_exists(self.rand_dir)

        old_q = join(self.pwd, self.query_fasta)
        self.new_rand_q = join(self.rand_dir, self.query_fasta)

        if not exists(self.new_rand_q):
            shutil.copy(old_q, self.new_rand_q)
        else:
            warn_exists(self.new_rand_q)

        old_rand = join(self.pwd, self.random_fasta)
        self.new_rand_db = join(self.rand_dir, self.random_fasta)

        if not exists(self.new_rand_db):
            shutil.move(old_rand, self.new_rand_db)
        else:
            warn_exists(self.new_rand_db)

    def find_query_like(self):
        """Use the threshold found in analyze_random to keep only top RNAs"""
        df = pd.read_csv(join(self.pwd, self.outDF), sep='\t', index_col=0)
        df = df[df['R value'] >= self.r_cut]
        return df


    def multiproc_data(self):
        pool = Pool(processes=2)
        random_data = (self.rand_dir,
                       self.new_rand_db,
                       self.new_rand_q,
                       self.frag_size,
                       self.k,
                       self.cores,
                       self.save_counts,
                       self.outDF,
                       self.outfile,
                       self.sd_cut,
                       False)

        real_data = (self.pwd,
                     self.database_fasta,
                     self.query_fasta,
                     self.frag_size,
                     self.k,
                     self.cores,
                     self.save_counts,
                     self.outDF,
                     self.outfile,
                     self.sd_cut,
                     True)

        random_and_real = [random_data, real_data]
        try:
            results = pool.map(frag_and_analyze, random_and_real)
        except:
            import sys
            import traceback
            raise Exception("".join(traceback.format_exception(*sys.exc_info())))

        self.r_cut = results[0]

    def run(self):
        """The main function for launching the process of finding top fragments"""
        print('Starting... Setting up directory structure'); stdout.flush()
        self.setup_random()
        self.multiproc_data()
        print('Finding lncRNAs above {} threshold'.format(self.r_cut)); stdout.flush()
        df = self.find_query_like()

        if self.out_topDF is not None:
            df.to_csv(join(self.pwd, self.out_topDF))
        return df

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('pwd')
    parser.add_argument('database_fasta')
    parser.add_argument('random_fasta')
    parser.add_argument('query_fasta')
    parser.add_argument('frag_size')
    parser.add_argument('outDF')
    parser.add_argument('outfile')
    parser.add_argument('out_topDF')
    parser.add_argument('-s', '--save_counts', action='store_true')
    parser.add_argument('-k', '--kmer', default='4')
    parser.add_argument('-sd', '--std_dev_cut', default='3')
    parser.add_argument('-c', '--cores', default='4')
    args = parser.parse_args()

    pipe = Pipeline(args.pwd,
                    args.database_fasta,
                    args.random_fasta,
                    args.query_fasta,
                    int(args.frag_size),
                    args.outDF,
                    args.outfile,
                    args.out_topDF,
                    args.save_counts,
                    int(args.kmer),
                    float(args.std_dev_cut),
                    int(args.cores))
    pipe.run()
