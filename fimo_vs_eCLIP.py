#!/usr/bin/env python

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import subprocess as sp
import pybedtools

from os.path import join
from itertools import count
from scipy.stats import ks_2samp
from scipy.stats import ttest_ind

import gtf
import kmer_counts

from my_tqdm import my_trange, my_tqdm

class BedMaker:

    def __init__(self, infasta=None, pwm_ls=None, gencode_file=None, eclip_bed=None,
                 outparent=None, outbed=None, out_intersect=None,
                 gencode_names=True, basepairs=150, threshold=.01, expressed=None):
        self.infasta = infasta
        self.pwm_ls = pwm_ls
        self.gencode_file = gencode_file
        self.eclip_bed = eclip_bed
        self.outparent = outparent
        self.outbed = outbed
        self.out_intersect = out_intersect
        self.gencode_names = gencode_names
        self.basepairs = basepairs
        self.threshold = threshold
        self.expressed = expressed

        self.fimo = pd.DataFrame()
        self.gencode_gtf = None
        self.eclip_df = None
        self.motif_regions_df = None
        self.intersect = None

    def _process_gencode_names(self, fimo):
        """Create new name columns from the GENCODE fasta header"""
        if self.gencode_names:
            func = lambda s: s.split('|')[0]
            fimo['transcript'] = fimo['sequence_name'].apply(func)
            func = lambda s: s.split('|')[4]
            fimo['common'] = fimo['sequence_name'].apply(func)
        #TODO how to handle if I'm not using gencode names
        return fimo

    def _filter_expressed(self, fimo):
        """Remove transcripts that are not in the expressed set"""
        #TODO how to handle if I'm not using gencode names
        if self.expressed is not None:
            fimo = fimo[fimo['common'].isin(self.expressed)]
        return fimo

    def single_fimo(self, pwm, i):
        """Run fimo, process the results, add results to self.fimo"""
        outdir = join(self.outparent, f'fimo{i}/')
        cmd = f'fimo --norc --thresh {self.threshold} --oc {outdir} {pwm} {self.infasta}'
        sp.run(cmd.split(), check=True)
        fimo_file = join(outdir, 'fimo.txt')
        fimo = pd.read_csv(fimo_file, sep='\t')

        fimo = self._process_gencode_names(fimo)
        fimo = self._filter_expressed(fimo)
        self.fimo = self.fimo.append(fimo, ignore_index=True)

    def make_fimo(self):
        """Process all the PWM files into a single DataFrame"""
        for i, pwm in enumerate(self.pwm_ls):
            self.single_fimo(pwm, i)

    def make_gencode_gtf(self):
        """Use GENCODE gtf to extract gene start/end data, so regions aren't expanded outside of gene bodies"""
        self.gencode_gtf = gtf.Maker(self.gencode_file, skiprows=5)
        self.gencode_gtf.append_attribute(col=1, name='transcript')
        cols = ['seqname', 'start', 'end', 'strand', 'transcript']
        self.gencode_gtf = self.gencode_gtf.expandedDF[cols]
        col_map = {'start':'gene start', 'end':'gene end', 'strand':'gene strand'}
        self.gencode_gtf = self.gencode_gtf.rename(columns=col_map)

    def merge_fimo_gencode(self):
        """Add gene data to fimo df and do bookkeeping (rename cols and change coordinate system)"""
        self.fimo = pd.merge(self.fimo, self.gencode_gtf, on='transcript')
        col_map = {'start':'motif start', 'stop': 'motif end'}
        self.fimo = self.fimo.rename(columns=col_map)
        self.fimo['start'] = self.fimo['gene start'] + self.fimo['motif start']
        self.fimo['end'] = self.fimo['gene start'] + self.fimo['motif end']
        cols = ['# motif_id', 'motif_alt_id', 'score', 'q-value']
        self.fimo = self.fimo.drop(cols, axis=1)

    def make_eclip_df(self):
        """Load ENCODE ECLIP data"""
        self.eclip_df = pd.read_table(self.eclip_bed, header=None)
        col_map = {0:'seqname', 1:'start', 2:'end', 5:'strand'}
        self.eclip_df = self.eclip_df.rename(columns=col_map)

    def calc_intersects(self):
        """For each predicted fimo motif, count intersections with ECLIP regions"""
        motifs_df = self.fimo[['seqname', 'start', 'end', 'gene strand']]
        motifs_df = motifs_df.rename(columns={'gene strand': 'strand'})
        motifs_bedtools = pybedtools.BedTool.from_dataframe(motifs_df)
        eclip_bedtools = pybedtools.BedTool.from_dataframe(self.eclip_df)
        self.intersect = motifs_bedtools.intersect(eclip_bedtools, c=True).to_dataframe()[['score']]
        self.intersect.columns = ['Intersect']
        if self.out_intersect is not None:
            self.intersect.to_csv(self.out_intersect)

    def _motif_region_start(self, row):
        center = (row['start'] + row['end']) / 2
        return int(max(row['gene start'], center-self.basepairs))

    def _motif_region_end(self, row):
        center = (row['start'] + row['end']) / 2
        return int(min(row['gene end'], center+self.basepairs))

    def make_motif_regions(self):
        """Expand fimo motifs from the center, keeping within gene body. Save to .bed file."""
        self.motif_regions_df = pd.DataFrame()
        self.motif_regions_df['seqname'] = self.fimo['seqname'].values
        self.motif_regions_df['start'] = self.fimo.apply(self._motif_region_start, axis=1)
        self.motif_regions_df['end'] = self.fimo.apply(self._motif_region_end, axis=1)
        self.motif_regions_df['name'] = self.fimo['transcript']
        self.motif_regions_df['score'] = 1
        self.motif_regions_df['strand'] = self.fimo['gene strand']
        self.motif_regions_df.to_csv(self.outbed, header=None, index=None, sep='\t')

    def bed4galaxy(self):
        self.make_fimo()
        self.make_gencode_gtf()
        self.make_eclip_df()
        self.merge_fimo_gencode()
        self.calc_intersects()
        self.make_motif_regions()


class Subsetter:
    """Responsible for handling a subset of False Positive regions, while controlling for overlap"""

    def __init__(self, inbed, intersect):
        self.inbed = inbed
        self.intersect = intersect

        self.tp_overlaps = self.calc_tp_overlap()
        self.tp_overlaps_mean = self.tp_overlaps.mean()
        self.fp_main_subset = self.calc_main_fp_subset()
        self.fp_overlaps_mean = None #not needed. Just for reporting

    def overlap(self, bed_df):
        chr_bed = pybedtools.BedTool.from_dataframe(bed_df)
        chr_intersect_df = chr_bed.intersect(chr_bed, c=True).to_dataframe()
        return chr_intersect_df.iloc[:, -1] - 1

    def calc_tp_overlap(self):
        tp_bed = self.inbed[self.intersect['Intersect'].astype(bool)]
        return self.overlap(tp_bed)

    def is_suitable(self, possible_main_sub):
        def not_sig(possible_main_sub):
            test_sample = possible_main_sub.sample(n=len(self.tp_overlaps))
            test_overlap = self.overlap(test_sample)
            ttest = ttest_ind(self.tp_overlaps.values, test_overlap.values)
            return ttest[1] > 0.1
        return all(not_sig(possible_main_sub) for i in range(5))

    def calc_main_fp_subset(self):
        fp_bed = self.inbed[~self.intersect['Intersect'].astype(bool)].copy()
        fp_bed[3] = np.arange(len(fp_bed)) #HACK We'll use these as unique identifiers to pull out counts for Analyzer.
        fp_overlap = self.overlap(fp_bed)
        fp_bed['overlap'] = fp_overlap.values
        fp_bed = fp_bed.sort_values('overlap', ascending=False)
        for size in range(min(70000, int(len(fp_bed)/2)), len(self.tp_overlaps), -50): # 70k is an arbitrary cap for SRSF1
            possible_main_sub = fp_bed.head(size)
            if self.is_suitable(possible_main_sub):
                print('true_overlap: ', self.tp_overlaps_mean)
                print('full_overlap: ', self.overlap(possible_main_sub).mean())
                print('sizes: ', len(self.tp_overlaps), len(possible_main_sub))
                print()
                return possible_main_sub.drop('overlap', axis=1)
        print('error', f'tp_mean: {self.tp_overlaps_mean} \nfp_means: {fp_mean_overlaps}')
        return None

    def fp_sim_mean(self, counts, cap):
        size = min(cap, len(self.tp_overlaps))
        sub = self.fp_main_subset.sample(size)
        counts_sub = counts[sub[3]]
        sim = np.corrcoef(counts_sub)
        sim = sim[np.triu_indices_from(sim, 1)]
        return sim.mean()

class Analyzer:

    def __init__(self, infasta=None, intersect=None, inbed=None, outgraph=None,
                 do_ks_test=True, k=6, n=1000, cap=1E5, quality='low'):
        self.infasta = infasta
        self.intersect = intersect
        self.inbed = inbed
        self.outgraph = outgraph
        #self.do_ks_test = do_ks_test
        self.k = k
        self.n = n
        self.cap = cap
        self.quality = quality

        if self.intersect is not None:
            self.intersect = pd.read_csv(self.intersect, index_col=0)
        if self.inbed is not None:
            self.inbed = pd.read_csv(self.inbed, header=None, sep='\t')
        self.subber = None
        self.counts = None
        self.tp_counts = None
        self.fp_counts = None
        self.tp_sim = None
        self.tp_sim_mean = None
        self.fp_sim_mean_ls = []
        self.fp_sim_mean = None
        self.fp_sim_std = None
        self.ks_pvals = None
        self.permu_pval = None

    def __repr__(self):
        s = (f'TP mean={self.tp_sim_mean}\n'
             f'FP mean={self.fp_sim_mean}\n'
             f'KS test p-values={self.ks_pvals}\n'
             f'Permu. test p-value={self.permu_pval}\n')
        return s

    def get_counts(self):
        counter = kmer_counts.BasicCounter(self.infasta, k=self.k)
        counter.get_counts()
        self.counts = counter.counts

    def tp_fp_stats(self):
        self.tp_counts = self.counts[self.intersect['Intersect'] != 0]
        self.fp_counts = self.counts[self.intersect['Intersect'] == 0]

        tp_sim = np.corrcoef(self.tp_counts)
        self.tp_sim = tp_sim[np.triu_indices_from(tp_sim, 1)]
        self.tp_sim_mean = self.tp_sim.mean()

        self.subber = Subsetter(self.inbed, self.intersect)
        for i in my_trange()(self.n):
            mean = self.subber.fp_sim_mean(self.fp_counts, self.cap)
            self.fp_sim_mean_ls.append(mean)
        # self.fp_sim_mean = np.mean(self.fp_sim_mean_ls)
        # self.fp_sim_std = np.std(self.fp_sim_mean_ls)

    def get_pvals(self):
        #if self.do_ks_test:
            #self.ks_pvals = [ks_2samp(self.tp_sim, self.random_corrs(False))[1] for i in range(10)]
        outlier = sum(1 for m in self.fp_sim_mean_ls if m > self.tp_sim_mean)
        self.permu_pval = outlier / self.n

    def plot(self):
        if self.quality == 'low':
            dpi = 72 #Add more
            title = False
            xlabel = 'log(mean(R value))'
            ylabel = 'Density'
            ticks = 24


        elif self.quality == 'high':
            dpi = 600
            xlabel = ''
            ylabel = ''
            ticks = 36


        if self.outgraph is not None:
            plt.semilogx()
            ax = sns.distplot(self.fp_sim_mean_ls, label='False\nPositives', kde_kws={'linewidth': 6})
            top = np.mean(ax.get_ylim())*1.5
            x = (self.tp_sim_mean, self.tp_sim_mean)
            plt.plot(x, (0, top), label='True\nPositive', linewidth=10)
            plt.ylabel(ylabel, fontsize=40)
            plt.yticks(fontsize=ticks)
            plt.xlabel(xlabel, fontsize=40)
            plt.xticks(fontsize=ticks)

            p = self.permu_pval if self.permu_pval != 0. else f'<{1/self.n}'
            plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
            #plt.title(p, fontsize=36)
            plt.savefig(self.outgraph, bbox_inches='tight', dpi=dpi)
            plt.clf()

    def run(self):
        self.get_counts()
        self.tp_fp_stats()
        self.get_pvals()
        self.plot()
