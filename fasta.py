# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 11:13:42 2015

@author: jessime

Warnings
--------
* A lot of this code is dependent on GENCODE formatted fasta files.
* The C code in the shuffle function will segfault if the string is too big.
This will cause the Python Kernel to crash.

Issues
------
1. Filters are too similar. Shouldn't repeat that code. Just send in the filter condition.
2. Maker and Extractor share code that maybe should be in parent class
3. Do `if outfile: save \ always return`
"""

import numpy as np
import pandas as pd
import pickle
import itertools
import random

from os.path import exists, join
from os import makedirs
from ushuffle import shuffle

from my_tqdm import my_tqdm, my_trange
from kmer_counts import BasicCounter, SingleCounter
from fasta_reader import Reader
from pearson import pearson


class Maker:
    """Manipulates fasta files into new fasta files.

    Parameters
    ----------
    infasta : str (default=None)
        Name of input fasta file to be manipulated
    outfasta : str (default=None)
        Location to store filtered fasta file
    outnames : str (default=None)
        Location to stores saved transcript names

    Attributes
    ----------
    data : list
        Zipped list of names and seqs tuples
    names : list
        Header lines of fasta file
    seqs : list
        Sequences from fasta file
    """

    def __init__(self, infasta=None, outfasta=None, outnames=None):
        self.infasta = infasta
        if infasta is not None:
            self.data, self.names, self.seqs = Reader(infasta).get_data()
        self.outfasta = outfasta
        self.outnames = outnames

    def _name_dump(self, common_name_list):
        """Save new names if necessary

        Parameters
        ----------
        common_name_list : list
            The common style names of the sequences which are being kept
        """
        if self.outnames is not None:
            pickle.dump(common_name_list, open(self.outnames, 'wb'))


    def filter1(self, zeros=1):
        """Filters a fasta file to only keep *1 transcript names.

        Parameters
        ----------
        zeros: int (default=1)
            The number of zeros preceding `1`.
            (e.g. zeros=1 will filter for `01` transcripts. zeros=2 will filter for `001`)

        Returns
        -------
        common_name_list : list
            The common style names of the sequences which are being kept
        """
        common_name_list = []
        with open(self.outfasta, 'w') as outfasta:
            for name, seq in self.data:
                common_name = name.split('|')[4]
                if common_name[-(zeros+1):] == '0'*zeros+'1':
                    common_name_list.append(common_name)
                    outfasta.write(name+'\n')
                    outfasta.write(seq+'\n')

        self._name_dump(common_name_list)
        return common_name_list

    def filter_size(self, size_lim, keep_all_below=True):
        """Filters fasta file based on size of each transcript

        Parameters
        ----------
            size_lim : int
                the size limit to keep above or below
            keep_all_below : bool (default=True)
                Keep sequences longer or shorter than specified length?

        Returns
        -------
        filtered_fasta : list
            Lines of the new fasta file
        """
        common_name_list = []
        filtered_fasta = []
        for name, seq in self.data:
            common_name = name.split('|')[4]
            length = len(seq)
            if ((length <= size_lim and keep_all_below) or
            (length >= size_lim and not keep_all_below)):
                if self.outnames:
                    common_name_list.append(common_name)
                filtered_fasta.append((name, seq))

        if self.outfasta is not None:
            with open(self.outfasta, 'w') as outfasta:
                for dt in filtered_fasta:
                    outfasta.write(dt+'\n')
        self._name_dump(common_name_list)
        return filtered_fasta

    def filter_name(self, keep_names):
        """Filters fasta file based on a list of common names

        Parameters
        ----------
        keep_names : list
            The common style names to be kept from the fasta file

        Returns
        -------
        filtered_fasta : list
            Lines of the new fasta file
        """
        common_names = [n.split('|')[4] for n in self.names]
        names_dict = dict(zip(common_names, self.data))
        filtered_fasta = []
        for n in keep_names:
            try:
                filtered_fasta.append(names_dict[n])
            except KeyError:
                print('{} is not in fasta file'.format(n))

        if self.outfasta is not None:
            with open(self.outfasta, 'w') as outfasta:
                for dt in filtered_fasta:
                    outfasta.write(dt[0]+'\n')
                    outfasta.write(dt[1]+'\n')
        return filtered_fasta

    def separate(self, outdir, filenames=None):
        """Separate a fasta file into individual fasta files

        Parameters
        ----------
        outdir : str
            Path of directory in which to save the new fasta files
        filenames : [str] (default=None)
            Optional list of names for files instead of integers.
        """
        if not exists(outdir):
            makedirs(outdir)

        if filenames is None:
            filenames = range(len(self.data))
        for fn, (name, seq) in zip(filenames, self.data):
            fn = '{}.fa'.format(fn)
            with open(join(outdir, fn), "w") as outfile:
                outfile.write(name+'\n')
                outfile.write(seq+'\n')


class RandomMaker(Maker):
    """Provides functions for creating randomly generated fasta files

    Parameters
    ----------
    infasta: str (default=None)
        Name of input fasta file to be manipulated
    outfasta: str (default=None)
        Location to store filtered fasta file
    k: int (default=1)
        The size of kmer to conserve between original and random sequences
    mutations: int
        Number of SNP mutations to make in sequence
    individual: bool (default=True)
        Whether to conserve kmers of each sequence or the entire fasta file

    Attributes
    ----------
    error: float
        The mean error in kmer frequency betwen original counts and new counts
    weights: DataFrame
        Shows fractions of each kmer for old and new seqs, along with error
    new_fasta_seqs: list
        New, randomized sequences with conserved kmer frequencies

    Notes
    -----
    The kmers are conserved using the uShuffle algorithm described here:
    http://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-9-192
    The C code can be found here:
    https://github.com/dcjones/cbgb/tree/master/ushuffle
    """

    def __init__(self, infasta=None, outfasta=None, k=1, mutations=0, individual=True):
        super(RandomMaker, self).__init__(infasta=infasta, outfasta=outfasta)
        self.k = k
        self.mutations = mutations
        self.individual = individual
        self.weights = None
        self.new_fasta_seqs = None

    def get_weights(self, seqs):
        """Get the ratio of the kmers in a set of sequences

        Returns
        -------
        weights : dict
            Keys are kmers, values are float ratios of the total count"""
        counter = SingleCounter(k=self.k)
        counter.seqs = seqs
        weights = counter.count_total()
        kmers = ["".join(i) for i in list(itertools.product('AGTC', repeat=self.k))]
        weights = {k:v for k,v in zip(kmers, weights)}
        return weights

    def get_single_weights(self, seq):
        """Get the ratio of kmers in a single sequence"""
        weights = np.array([seq.count(b) for b in 'AGTC'], dtype=np.float64)
        weights /= weights.sum()
        return weights

    def shuffle(self, seq):
        """Makes a kmer conserved random sequence."""
        # if self.k == 1:
        #     seq = list(seq)
        #     random.shuffle(seq) # Need seperate line because shuffle occurs inplace
        #     rand_seq = ''.join(seq)
        # else:
        rand_seq = shuffle(seq.encode(), self.k).decode('utf-8')
        rand_array = np.array(list(rand_seq))
        indices = np.random.choice(len(seq), self.mutations, replace=False)
        new_bases = np.random.choice(list('AGTC'), self.mutations)
        rand_array[indices] = new_bases
        rand_seq = ''.join(rand_array)
        return rand_seq

    def get_random_seqs(self, seqs):
        """Generates random RNA sequences

        Parameters
        ----------
        seqs : [str]
            Original sequences to create random copies of

        Returns
        -------
        new_seqs : [str]
            Randomly generated sequences
        """
        new_seqs = [self.shuffle(s) for s in my_tqdm()(seqs)]
        return new_seqs

    def split(self, seq):
        """Return concatenated sequence to individual sequence lengths"""
        lengths = [len(s) for s in self.seqs]
        new_seqs = []
        for l in lengths:
            new_seqs.append(seq[:l])
            seq = seq[l:]
        return new_seqs

    def inject_seqs(self, new_seqs):
        """Use the original fasta file headers to put the random sequences in fasta format

        Parameters
        ----------
        new_seqs : list
            string elements, each a randomly generated RNA

        Returns
        -------
        new_fasta_seqs : list
            The lines of a new fasta file filled with random RNAs
        """
        new_fasta_seqs = []
        for name, new_seq in zip(self.names, new_seqs):
            new_fasta_seqs.append(name)
            new_fasta_seqs.append(new_seq)
        return new_fasta_seqs

    def save(self):
        if self.outfasta is not None:
            with open(self.outfasta, 'w') as outfasta:
                for line in self.new_fasta_seqs:
                    outfasta.write('{}\n'.format(line))

    def synthesize_random(self):
        """Make random RNAs based on natural RNAs.

        Returns
        -------
        new_fasta_seqs : list
            Lines of the new fasta file
        """
        if self.individual:
            new_seqs = self.get_random_seqs(self.seqs)
        else:
            new_seqs = self.get_random_seqs([''.join(self.seqs)])
            new_seqs = self.split(new_seqs[0])
        self.new_fasta_seqs = self.inject_seqs(new_seqs)
        self.save()
        return self.new_fasta_seqs


class NegativeMaker():
    """Makes random RNAs that are negatively correlated with an RNA of interest

    Unfortunately, this is just done totally at random and then the top 'n' RNAs are kept.
    It's a brute force approach, so expect it to run for a while.

    Parameters
    ----------
    num_ratios : int
        Number of nucleotide ratios to generate (outer loop)
    num_seqs : int
        Number of sequences to generate from each nucleotide ratio (inner loop)
    num_return : int
        Nuber of sequences to return
    seq_len : int
        Length of each sequence generated
    """

    def __init__(self, query_seq=None, outfasta=None, mean=None, std=None, seq_len=None,
                 num_ratios=100000, num_seqs=1000, num_return=100):
        self.query_seq = query_seq
        self.outfasta = outfasta
        self.num_ratios = num_ratios
        self.num_seqs = num_seqs
        self.num_return = num_return
        self.seq_len = seq_len

        if mean is not None:
            self.mean = np.load(mean)
        if std is not None:
            self.std = np.load(std)

        self.results = {}
        self.query_counts = None
        self.ratios = self.make_ratios()
        self.max_min = None

    def set_query_counts_and_seq_len(self):
        """Find the normalized array for the query sequence"""
        query_seq = Reader(self.query_seq).get_seqs()
        if self.seq_len is None:
            self.seq_len = len(query_seq[0])
        self.query_counts = self.get_normed_counts(query_seq)

    def make_ratios(self):
        ratios = np.random.randint(0, 10000, 4).astype(np.float)
        return ratios / ratios.sum()

    def make_seq_batch(self):
        seqs = []
        nucs = np.array(list('AGTC'))
        for i in range(self.num_seqs):
            rand = np.random.choice(nucs, size=self.seq_len, p=self.ratios)
            seqs.append(''.join(rand))
        return seqs

    def get_normed_counts(self, seqs):
        """Get kmer counts and normalize them"""
        counter = BasicCounter(centered=False, standardized=False, silent=True)
        counter.seqs = seqs
        counter.get_counts()
        counter.counts -= self.mean
        counter.counts /= self.std
        return counter.counts

    def initial_batch(self):
        """Fills the results with a number of possible sequences"""
        seqs = self.make_seq_batch()
        counts = self.get_normed_counts(seqs)
        dist = np.squeeze(pearson(self.query_counts, counts))
        self.results = dict(zip(dist, seqs))
        self.max_min = max(self.results)

    def synthesize_random(self):
        self.set_query_counts_and_seq_len()
        self.initial_batch()

        for i in my_trange()(self.num_ratios):
            self.ratios = self.make_ratios()
            seqs = self.make_seq_batch()
            counts = self.get_normed_counts(seqs)
            dist = np.squeeze(pearson(self.query_counts, counts))
            for d, s in zip(dist, seqs):
                if d < self.max_min:
                    del self.results[self.max_min]
                    self.results[d] = s
                    self.max_min = max(self.results)
                    print((i, np.array(self.results.keys()).mean()), end='\r')


class Extracter(object):
    """Extracts information from fasta format into other data/file types

    Parameters
    ----------
    infasta : str
        Name of input fasta file to be manipulated
    outfasta : str
        location to store extracted data from infasta

    Attributes
    ----------
    data : list
        Zipped list of names and seqs tuples
    names : list
        Header lines of fasta file
    seqs : list
        Sequences from fasta file
    """

    def __init__(self, infasta=None, outfile=None):
        if infasta is not None:
            self.data, self.names, self.seqs = Reader(infasta).get_data()
        self.outfile = outfile

    def save_list(self, ls):
        if self.outfile is not None:
            pickle.dump(ls, open(self.outfile, 'wb'))

    def get_names(self, method):
        """Dump all the sequence names to a text file.

        Parameters
        ----------
        method : str
            An easy to remember descriptor for each name type in headers.
            ['ensembl', 'ensembl_gene', 'havana_gene', 'havana', 'common', 'common_gene']

        Returns
        -------
        names_ls : list
            Names of each sequence in the given style
        """
        name_loc = {"ensembl": 0,
                    "ensembl_gene": 1,
                    "havana_gene": 2,
                    "havana": 3,
                    "common": 4,
                    "common_gene": 5}
        index = name_loc[method]
        names_ls = [name.split('|')[index].strip('>') for name in self.names]
        self.save_list(names_ls)
        return names_ls

    def get_gffread_names(self, spliced):
        """Parses headers created from gffread and tags transcripts as spliced or unspliced

        Parameters
        ----------
        spliced: bool
            Names will end with '-SP' if True, and '-UN' otherwise.

        Returns
        name_ls : list
            New names of each transcript
        """
        suffix = '-SP' if spliced else '-UN'
        names_ls = [n.split('=')[1]+suffix for n in self.names]
        self.save_list(names_ls)
        return names_ls

    def get_lengths(self):
        """Measures the length of each sequence.

        Returns
        -------
        lengths : list [int]
            int lengths for self.seqs
        """

        lengths = [len(s) for s in self.seqs]
        self.save_list(lengths)
        return lengths

    def gc_content(self):
        """Calculates GC content of each sequence

        Returns
        -------
        gc : list [int]
            GC content as a ratio.
        """
        gc = [(s.count('C')+s.count('G'))/len(s) for s in self.seqs]
        self.save_list(gc)
        return gc
