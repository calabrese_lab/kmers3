#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  9 10:33:33 2016

@author: jessime

Stumbling Blocks
----------------

1. Don't have parentheses in headers. Or "/" characters.
2. The query frag must be the last fragment of infasta (or set query_index).
3. Frags must have unique names

Description
-----------

This is a notebook dump of a couple functions that Mauro is having me run on experimental data.
I'll have to run it multiple times, so it should have it's own script.
Here's what it does according to what Mauro wants:

Sue still needs to repeat the assays but she got another round of tetris data and I would like to re-examine things and see where we are with predictions.
I've added a few new lncRNAs to the attached file since last months query. Also, wondering if this time, we can compare everything to "xist-minimal², which is the region that Kaoru defined as the minimal sequence necessary to get maximal repression, instead of Xist-2kb.
Like you did about a month ago, it would be useful to see three comparisons:
1) the standardized similarity of each fragment to the minimal fragment of Xist
2) the standardized similarity of the minimal fragment within each sequence that is most similar to the minimal fragment of Xist. This is the domain-based comparison method.
3) the top needleman alignment score normalized to the score for a perfect match to the minimal Xist fragment
"""

import os
import numpy as np
import pandas as pd

from subprocess import check_call

import fasta_reader
import fasta
import kmer_counts
import pearson
import frag_query
import alignment

from my_tqdm import my_tqdm

class Tetris:
    """A pipeline of analyses we are interested in for TETRIS fragments.

    Parameters
    ----------
    infasta : str
        Path to TETRIS fragment, including the query sequence of interest
    mean : str
        Path to numpy array containing a known mean of a population of sequences
    std : str
        Path to numpy array containing a known std. dev of a population of sequences
    out_dist : str
        Path to comparison between query sequence and all full length sequences in infasta
    out_frags : str
        Path to results from frag_query
    out_align : str
        Path to results from nhmmer and strecher runs
    outdir : str
        Path to dir (can be made) to store intermediate results
    out_counts : str (default=None)
        Path to kmer counts. If not specficied, kmer counts are not saved.
    query_index : int (default=-1)(wt_diff <= -1).sum(axis=1).head()lt=None)
        Kmers to use for comparison in whole_seq measurement

    TODO: update docs

    Attributes
    ----------
    headers : [str]
        These usually aren't full ENCODE style headers, just names
    infasta1 : str
        Path to intermediate file for query seq
    infasta2_base : str
        Formattable path to intermediate fasta files
    counter : BasicCounter
        Makes and stores kmer counts
    align_df : DataFrame
        Stores nhmmer and strecher data

    Note
    ----
    See module for motivation description
    """
    def __init__(self, infasta, mean, std, outdir, outfile=None, out_counts=None,
                 whole_seq=True, frag_seq=True, nhmmer=True, stretcher=True,
                 query_index=-1, n=None, k=4, kmer_subset=None, silent=False):
        self.infasta = infasta
        self.mean = mean
        self.std = std
        self.outdir = outdir
        self.outfile = outfile

        self.whole_seq = whole_seq
        self.frag_seq = frag_seq
        self.nhmmer = nhmmer
        self.stretcher = stretcher
        self.out_counts = out_counts
        self.query_index = query_index
        self.n = n
        self.k = k
        self.kmer_subset = kmer_subset
        self.silent = silent

        if self.n is None:
            self.n = self.calc_n()
        #TODO let mean and std default to data/ path

        self.headers = self.get_headers()
        self.infasta1 = os.path.join(self.outdir, '{}.fa'.format(self.headers[self.query_index]))
        self.infasta2_base = os.path.join(self.outdir, '{}.fa')
        self.counter = kmer_counts.BasicCounter(self.infasta,
                                                self.out_counts,
                                                k=self.k,
                                                mean=False,
                                                std=False,
                                                silent=silent)

        self.align_df = pd.DataFrame(index=self.headers)

        self.sub_index = None
        if kmer_subset is not None:
            self.sub_index = self.calc_kmer_sub_index()

    def calc_n(self):
        """Find length of query seq.

        Returns
        -------
        n : int
            Length in bp of sequence found at self.query_index in self.infasta"""
        seqs = fasta_reader.Reader(infasta=self.infasta).get_seqs()
        n = len(seqs[self.query_index])
        return n

    def calc_kmer_sub_index(self):
        """Find index of kmers to extract from full set for measurement

        Returns
        -------
        index : array
            indicies of kmers of interest
        """
        index = [i for i,k in enumerate(self.counter.kmers) if k in self.kmer_subset]
        index = np.array(index)
        return index

    def get_headers(self):
        """Get full names of sequences

        Notes
        -----
        Mauro has been putting parentheses in headers, which Strecher doesn't like.
        I'm not going to change it here, but I'll raise an error for them.

        Returns
        -------
        headers : [str]
            These usually aren't full ENCODE style headers, just names
        """
        headers = fasta_reader.Reader(infasta=self.infasta).get_headers()
        for i, h in enumerate(headers):
            if '(' in h or ')' in h:
                err = ('Headers cannot contain parentheses.'
                       'Please change header {}: "{}".')
                raise ValueError(err.format(i, h))
        headers = [h.strip('>') for h in headers]
        return headers

    def save_counts(self):
        if self.counter.outfile is not None:
            self.counter.binary = False
            self.counter.label = True
            self.counter.save(self.headers)

    def make_kmer_counts(self):
        mean = np.load(self.mean)
        std = np.load(self.std)
        self.counter.get_counts()
        if self.sub_index is not None:
            mean = mean[self.sub_index]
            std = std[self.sub_index]
            self.counter.counts = self.counter.counts[:, self.sub_index]
        self.counter.counts -= mean
        self.counter.counts /= std

        self.save_counts()

    def make_dist(self):
        new_shape = 4**self.k
        if self.sub_index is not None:
            new_shape = len(self.sub_index)
        query_seq = np.reshape(self.counter.counts[self.query_index], [1, new_shape])
        r_whole = pearson.pearson(query_seq, self.counter.counts)[0]
        self.align_df['R_whole'] = r_whole

    def separate(self):
        if not os.path.exists(self.outdir):
            os.makedirs(self.outdir)
        maker = fasta.Maker(self.infasta)
        maker.separate(self.outdir, self.headers)

    def run_frag_query(self):
        outfile_base = os.path.join(self.outdir, '{}.csv')
        for rna in my_tqdm()(self.headers):
            infasta2 = self.infasta2_base.format(rna)
            outfile = outfile_base.format(rna)
            analyzer = frag_query.Analyzer(self.infasta1,
                                           infasta2,
                                           self.mean,
                                           self.std,
                                           outfile,
                                           self.n,
                                           1,
                                           k=self.k,
                                           silent=True)
            analyzer.run()

    def aggregate_frag_query(self):
        infile_base = os.path.join(self.outdir, '{}.csv')
        df = pd.read_csv(infile_base.format(self.headers[0]), sep='\t', index_col=0)
        df.index = self.headers[:1]
        for rna in self.headers[1:]:
            new = pd.read_csv(infile_base.format(rna), sep='\t', index_col=0)
            new.index = [rna]
            df = df.append(new)
        self.align_df['R_seg'] = df['R_value']
        self.align_df['Pos'] = df['Pos']

    def run_nhmmer(self):
        outfile_base = os.path.join(self.outdir, '{}_nhmmer.txt')
        for rna in self.headers:
            infasta2 = self.infasta2_base.format(rna)
            outfile = outfile_base.format(rna)
            cmd = 'nhmmer --nonull2 --nobias --noali -o {} {} {}'
            cmd = cmd.format(outfile, self.infasta1, infasta2).split()
            check_call(cmd)

    def aggregate_nhmmer(self):
        infile_base = os.path.join(self.outdir, '{}_nhmmer.txt')
        results = []
        for rna in self.headers:
            infile = infile_base.format(rna)
            with open(infile) as infile:
                data = infile.readlines()
                line = data[18]
                if line.strip():
                    score = float(line.split()[1])
                    results.append(score)
                else:
                    results.append(0.0)

        self.align_df['Score'] = results
        self.align_df['Norm'] = self.align_df['Score']/self.align_df['Score'].max()

    def run_stretcher_and_aggregate(self):
        outfile_base = os.path.join(self.outdir, '{}_stretcher.txt')
        results = []
        for rna in self.headers:
            infasta2 = self.infasta2_base.format(rna)
            outfile = outfile_base.format(rna)
            stretch = alignment.Emboss('stretcher', self.infasta1, infasta2, outfile)
            stretch.align()
            results.append(stretch.parse_results()[0])

        self.align_df['Stretcher'] = results

    def run(self):
        if not self.silent:
            print('Finding counts')
        self.make_kmer_counts()

        if self.whole_seq:
            if not self.silent:
                print('Calculating whole-seq R-values')
            self.make_dist()

        if self.frag_seq:
            if not self.silent:
                print('Searching for fragments.')
            self.separate()
            self.run_frag_query()
            self.aggregate_frag_query()

        if self.nhmmer:
            if not self.silent:
                print('Running nhmmer')
            if not self.frag_seq:
                self.separate()
            self.run_nhmmer()
            self.aggregate_nhmmer()

        if self.stretcher:
            if not self.silent:
                print('Running stretcher')
            self.run_stretcher_and_aggregate()

        if self.outfile is not None:
            self.align_df.to_csv(self.outfile)
