#!/bin/bash


#SBATCH -n 16
#SBATCH -t 3:00:00
#SBATCH --mem 72G

module load salmon

cd $1
echo $PWD
gtf="../../v26_full_unspliced.gtf"
genome="/proj/seq/data/STAR_genomes/GRCh38_p10_GENCODE"
STAR --sjdbGTFfile $gtf \
     --quantMode TranscriptomeSAM \
     --genomeDir $genome \
     --outSAMtype BAM Unsorted \
     --runThreadN 16 \
     --readFilesCommand zcat \
     --readFilesIn $2

fasta="../../v26_full_unspliced.fa"
bam="Aligned.toTranscriptome.out.bam"
salmon quant -l A -t $fasta -a $bam -o salmon
