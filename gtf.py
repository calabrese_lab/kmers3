# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 11:04:41 2016

@author: jessime

Description
-----------
Handles General Transfer Files (GTF)

Issues
------
I haven't bothered to name a majority of the columns in the expanded DataFrame.
They can be added as necessary.

More functionality could go into the Reader
"""

import pandas as pd
import numpy as np

from pickle import load
from csv import QUOTE_NONE
from collections import defaultdict

from my_tqdm import my_tqdm

class Reader:

    def __init__(self, inGTF, skiprows=None):
        self.inGTF = inGTF
        if inGTF is not None:
            self.inGTF = self.get_GTF(inGTF, skiprows)

        self.data = None

    def get_GTF(self, inGTF, skiprows):
        """Load the gtf has a dataframe"""
        gtf = pd.read_csv(inGTF, sep='\t', skiprows=skiprows, header=None)
        gtf = gtf.rename(columns={0:'seqname',
                                  1:'source',
                                  2:'feature',
                                  3:'start',
                                  4:'end',
                                  5:'score',
                                  6:'strand',
                                  7:'frame',
                                  8:'attribute'})
        return gtf

class Maker:
    """Manipulates gtf files into new gtf files."""

    def __init__(self, inGTF=None, outGTF=None, skiprows=None):
        # TODO Allow default_data to load chr_sizes

        self.inGTF = Reader(inGTF, skiprows).inGTF
        self.outGTF = outGTF

        self.featureDF = None
        self.expandedDF = None

    def filter_feature(self, feature='transcript'):
        """Keeps only transcript rows, as desginated by the 'feature' column"""
        self.featureDF = self.inGTF[self.inGTF.feature == feature]

    def expand_geneid(self):
        """Gives gene_id field in attribute column its own column in self.expandedDF"""
        geneids = []
        for i, row in self.inGTF.iterrows():
            gene = row.attribute.split(';')[0].split()[1].strip('\"')
            geneids.append(gene)
        self.expandedDF = self.inGTF.copy()
        self.expandedDF['Gene_id'] = geneids

    def expand_transcripts(self, feature='transcript'):
        """Separate the 'attributes' column into separate series"""
        print('Warning: this method is depreciated. Use `append_attribute` instead')
        if self.featureDF is None:
            self.filter_feature(feature)
        self.expandedDF = self.featureDF.attribute.str.split(';').apply(pd.Series)
        self.expandedDF = pd.concat([self.featureDF, self.expandedDF], axis=1)
        self.expandedDF = self.expandedDF.rename(columns={5:'transcript_name'}) #NOTE column number is 7 in py2 version.
        self.expandedDF.transcript_name = self.expandedDF.transcript_name.str.extract('"(.*?)"', expand=False)

    def append_attribute(self, col=7, name=None, feature='transcript'):
        """Add an attribute from the attributes column as its own column in self.expandedDF

        Parameters
        ----------
        col : int (default=7)
            Column number inside of the attribute column
        name : str | None (default=None)
            New name of column
        feature : str (default=transcripts)
            Feature name to be passed to `filter_feature` method
        """
        def read_attribute(attr, col):
            data = attr.split(';')[col]
            return data.split()[1].strip('"')

        if self.featureDF is None:
            self.filter_feature(feature)
        if name is None:
            name = self.featureDF.iloc[0]['attribute'].split(';')[col].split()[0]

        new = self.featureDF['attribute'].apply(read_attribute, args=(col,))

        if self.expandedDF is None:
            self.expandedDF = self.featureDF.copy()
        self.expandedDF[name] = new

    def save(self, df):
        """Saves dataframe to file if outGTF is provided"""
        if self.outGTF  is not None:
            df.to_csv(self.outGTF,
                      sep='\t',
                      index=False,
                      header=False,
                      quoting=QUOTE_NONE)

    def filter_01(self, feature='transcript'):
        """Filters a gtf file to only keep 01 transcript names."""
        if feature is not None:
            self.filter_feature(feature)
        self.expand_transcripts()
        condition = self.expandedDF.transcript_name.str.endswith('01')
        transcripts01DF = self.featureDF[condition]

        self.save(transcripts01DF)
        return transcripts01DF

    def filter_names(self, keep_names):
        """Filters gtf file based on a list of common names"""
        if self.featureDF is not None:
            self.filter_feature()
        if self.expandedDF is not None:
            self.expand_transcripts()
        condition = self.expandedDF.transcript_name.isin(keep_names)
        keep_namesDF = self.featureDF[condition]

        self.save(keep_namesDF)
        return keep_namesDF

    def _double_dataframe(self):
        """Make an empty dataframe, twice the length of self.inGTF"""
        new_index = range(len(self.inGTF)*2)
        new_columns = self.inGTF.columns
        newDF = pd.DataFrame(index=new_index, columns=new_columns)
        return newDF

    def _set_left(self, row, size, shift, chr_sizes):
        """Calculate the new region to the left of the original"""
        left = row.copy()
        left.end = max(left.start - shift, 1)
        left.start = max(left.start - size - shift, 1)
        return left

    def _set_right(self, row, size, shift, chr_sizes):
        """Calculate the new region to the right of the original"""
        right = row.copy()
        right.start = min(row.end + shift, chr_sizes[row.seqname])
        right.end = min(right.end + size + shift, chr_sizes[row.seqname])
        return right

    def only_surrounding_regions(self, size, chr_sizes, shift=0, tss=False):
        """Replaces current regions with new regions to the left/right

        Parameters
        ----------
        size : int or str
            The number of basepairs to make the surrounding regions.
            If set to 'locus', (end - start) will be used for each region
        chr_sizes : str
            File location of chromosome sizes
        shift : int (default=0)
            Moves the surrounding regions away an additional number of basepairs.
        tss : bool (default=False)
            If True, the 'end' column is set to the 'start' column.
        """
        chr_sizes = load(open(chr_sizes, 'rb'))
        use_locus_size = size == 'locus'
        if tss:
            self.inGTF.end = self.inGTF.start

        newDF = self._double_dataframe()

        for i, row in my_tqdm()(self.inGTF.reset_index().iterrows()):
            if use_locus_size:
                size = row.end - row.start
            newDF.ix[2*i] = self._set_left(row, size, shift, chr_sizes)
            newDF.ix[(2*i)+1] = self._set_right(row, size, shift, chr_sizes)

        self.save(newDF)
        return newDF

    def expand_regions(self, size, chr_sizes):
        """Adds and additional length to left/right of current regions

        Parameters
        ----------
        size : int or str
            The number of basepairs to make the surrounding regions.
            If set to 'locus', (end - start) will be used for each region
        chr_sizes : str
            File location of chromosome sizes
        """
        chr_sizes = load(open(chr_sizes, 'rb'))
        newDF = self.inGTF.copy()
        newDF['start'] = newDF['start'].map(lambda x: max(x - size, 1))
        newDF['end'] = newDF.apply(lambda x: min(x['end']+size, chr_sizes[x['seqname']]), axis=1)

        self.save(newDF)
        return newDF

    def intersection_count(self, GTF2, skiprows=None, match_strand=False):
        """Counts regions in GTF2 that intersect with each region in inGTF."""
        print('WARNING: This function is depreciated. Use pybedtools to count intersections')
        if not isinstance(GTF2, pd.DataFrame):
            GTF2 = self.get_GTF(GTF2, skiprows=skiprows)
        counts = np.zeros(len(self.inGTF))
        current_chr = None
        for i, (name, row) in my_tqdm()(enumerate(self.inGTF.iterrows())):
            if row.seqname != current_chr:
                gtf_chr = GTF2[GTF2.seqname == row.seqname]
                current_chr = row.seqname
            # There are four ways you can intersect a window (row)
            #         |________________________| *Window of interest*
            # 1.  |___________| End in
            # 2.  |_______________________________| Encapsulating
            # 3.             |______| Encapsulted
            # 4.                       |__________|  Start in
            # The conditions before the logical 'or' take care of 1. and 2.
            # Conditions after the logical 'or' take care of 3. and 4.
            gtf_intersect = gtf_chr[((gtf_chr.start <= row.start) &
                                     (row.start <= gtf_chr.end)) |
                                    ((row.start <= gtf_chr.start) &
                                     (gtf_chr.start <= row.end))]

            if match_strand:
                gtf_intersect = gtf_intersect[gtf_intersect.strand == row.strand]
            counts[i] = len(gtf_intersect)

        intersect = pd.DataFrame(data=counts,
                                 index=self.inGTF.index,
                                 columns=['Intersect'])
        return intersect

class Extracter:
    """Extracts information from GTF format into other data/file types

    """
    def __init__(self, inGTF=None, outfile=None, skiprows=None):
        self.inGTF = Reader(inGTF, skiprows).inGTF
        self.outfile = outfile

    def id_and_name(self, row):
        row_ls = row.attribute.split(';')
        gene_id = row_ls[0].split()[1].strip('\"')
        transcript_id = row_ls[1].split()[1].strip('\"')
        name = DataFramerow_ls[5].spl_namesit()[1].strip('\"')
        return transcript_id, (gene_id, name)

    def transcripts_id2name(self):
        transcripts = self.inGTF[self.inGTF.feature == 'transcript']
        id_name_map = dict([self.id_and_name(row) for i, row in transcripts.iterrows()])
        df = pd.DataFrame.from_dict(id_name_map, orient='index')
        df.columns = ['gene_id', 'common']
        if self.outfile is not None:
            df.to_csv(self.outfile)
        return df

    def intersection_lookup(self, GTF2, skiprows=None):
        """Finds regions in GTF2 that intersect with each region in inGTF.

        Parameters
        ----------
        GTF2 : str

        Returns
        -------
        lookup : {str:set}
            Transcript ids as keys and a set of protein gene ids as values.
        """
        def extract_geneid(row):
            gene_id = row['attribute'].split(';')[0].split()[1].strip('\"')
            return gene_id

        GTF2 = Reader(GTF2, skiprows).inGTF
        GTF2 = GTF2[GTF2['feature'] == 'gene']
        GTF2['Gene_id'] = GTF2.apply(extract_geneid, axis=1)

        lookup = defaultdict(set)
        current_chr = None
        for i, row in my_tqdm()(self.inGTF.iterrows()):
            if row['seqname'] != current_chr:
                gtf_chr = GTF2[GTF2['seqname'] == row['seqname']]
                current_chr = row['seqname']
            gtf_intersect = gtf_chr[((gtf_chr['start'] <= row['start']) &
                                     (row['start'] <= gtf_chr['end'])) |
                                    ((row['start'] <= gtf_chr['start']) &
                                     (gtf_chr['start'] <= row['end']))]
            intersect_names = set(gtf_intersect['Gene_id'])
            lookup[row['transcript_name']].update(intersect_names)
        return lookup
