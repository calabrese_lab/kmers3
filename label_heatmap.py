# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 14:22:17 2016

@author: jessime
"""

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt

import lineid_plot

from PIL import Image

def color_text_boxes(ax, labels, colors, color_arrow=True):
    assert len(labels) == len(colors), \
        "Equal no. of colors and lables must be given"
    boxes = ax.findobj(mpl.text.Annotation)
    box_lables = lineid_plot.unique_labels(labels)

    for box in boxes:
        l = box.get_label()
        try:
            loc = box_lables.index(l)
        except ValueError:
            continue  # No changes for this box
        box.set_color(colors[loc])
        if color_arrow:
            box.arrow_patch.set_color(colors[loc])

    ax.figure.canvas.draw()

def read_names(infile):
    """Read lncRNAs from Treeview output text file"""
    with open(infile) as infile:
        names = [l.strip() for l in infile.readlines()]
    return names

def make_label_lists(label_dict, all_names, width, padding):
    """Returns the lists needed for labeling the heatmap"""
    labels = []
    locs = []
    colors = []

    length = float(len(all_names))
    for color, names in label_dict.items():
        labels += names
        locs += [all_names.index(n)/length for n in names]
        colors += [color]*len(names)

    labels = [i[:-4] if i.endswith('01') else i for i in labels]
    locs = [(i * width) + padding for i in locs]
    return labels, locs, colors

def main(label_dict, innames, image, outfile,
         fontsize=16, max_iter=1000, padding=0):
    """

    Parameters
    ----------
    label_dict : dict {color : [names]}
        Groups lists of names by color. Each name in the list will be labeled in the given color.
    innames : list [str]
        Ordered names of all possible labels. (Can be exported from TreeView).
    image : str
        Path to the heatmap to be labeled.
    outfile : str
        Path to the new, labeled image
    fontsize : int (default=16)
        Size of the labels
    max_iter : int (default=1000)
        Number of iterations to run in finding an optimal label placement
    padding : int (default=0)
        Number of pixels of whitespace above top of heatmap.
    """
    plt.style.use('classic')

    all_names = read_names(innames)

    image = np.rot90(np.array(Image.open(image)))
    heatmap_width = image.shape[1] - (2*padding)
    ax = plt.imshow(image)

    labels, locs, colors = make_label_lists(label_dict, all_names, heatmap_width, padding)

    x = np.linspace(0, image.shape[1], 1000)
    y = np.zeros(x.shape[0])
    fig, ax = lineid_plot.plot_line_ids(x, y, locs, labels,
                                        extend=False,
                                        label1_size=fontsize,
                                        box_axes_space=-.2,
                                        ax=ax.axes,
                                        max_iter=max_iter)

    plt.xticks([])
    plt.yticks([])
    color_text_boxes(ax.axes, labels, colors)
    boxes = ax.axes.findobj(mpl.text.Annotation)
    plt.savefig(outfile, bbox_extra_artists=boxes, bbox_inches='tight')


class CommunityDistros:

    def __init__(self, names=None, groups=None, out_path=None, stable_only=True, colors=None, flip=False):
        """Plot the distributions of network communities relative to heatmap."""
        self.names = names
        self.groups = groups
        self.out_path = out_path
        self.stable_only = stable_only
        self.colors = colors
        self.flip = flip
        self.group_distros = None

    def read_names(self):
        """Return list of names from TreeView."""
        with open(self.names) as infile:
            names = [n.strip() for n in infile]
            if self.flip:
                names = names[::-1]
        return names

    def set_groups(self):
        groups = pd.read_csv(self.groups, index_col=0)
        if self.stable_only:
            groups = groups[groups['stable'] == True]
        return groups

    def find_indices(self):
        n_communities = len(self.groups['group'].unique())
        group_distros = []
        name_set = set(self.names)  #Checking if 'n in name_set' allows for subsetting transcripts.
        for i in range(n_communities):
            df = self.groups[self.groups['group'] == i]
            locs = [self.names.index(n) for n in df.index if n in name_set]
            group_distros.append(locs)
        return group_distros

    def make_plot(self):
        n_communities = len(self.groups['group'].unique())
        plt.subplots(figsize=[6,9])
        for i, locs in enumerate(self.group_distros):
            color = self.colors[i] if self.colors is not None else None
            sns.distplot(locs, vertical=True, color=color, label=str(i))
        plt.ylim([0, len(self.names)])
        plt.xticks([])
        plt.yticks([])
        if self.colors is None:
            print('WARNING: skipping setting labels since colors is not set.')
            if self.out_path is not None:
                plt.savefig(self.out_path, bbox_inches='tight', dpi=600)
            plt.show()
            return
        handles = []
        for i in range(n_communities - 1):
            patch = mpl.patches.Patch(color=self.colors[i], label=str(i))
            handles.append(patch)
        plt.legend(handles=handles, loc='center left', bbox_to_anchor=(1, 0.5))
        if self.out_path is not None:
            plt.savefig(self.out_path, bbox_inches='tight', dpi=600)
        plt.show()

    def plot(self):
        self.names = self.read_names()
        self.groups = self.set_groups()
        self.group_distros = self.find_indices()
        self.make_plot()
