#!/usr/bin/perl -w
use strict;

#8/11/15
# mk mrna sequences from ulitsky file, specifically for gg4

#11/15/13
# change input format slightly to match file
# runs on codon
# matches 1-based, inclusive coordinate regime of gtf files

#3/3/09
#1/15/09
# to take list of gene names and exon coords and make a .fa file with the
# DNA sequence.  Need access to copy of mouse genome.

my($in_genes, $in_genome, $outname)=@ARGV;

my $genome=store_genome($in_genome);
my ($gene_hoa) = collapse_genes($in_genes);
my $seqlist=make_seqs($gene_hoa, $genome);
printout($seqlist, $outname);

###   SUBS   ###
sub store_genome {
    my $in = shift;
    open (IN, "$in") or die "no infile $in given\n";
    my $fasta;
    my $id;
    my %seqs;
    my $tot;
    my $once;
    my $count;
    
    while (my $line=<IN>) {
	chomp $line;
	
	if ($line!~ /^>/) {
	    $fasta.="$line";
	} elsif ($line=~/^>/) {
	    $tot++;
	    
	    if (!defined $id) {
		$line=~s/^>//;
		$id=$line;
		$once++;
	    } else {
		
		$seqs{$id}=$fasta;
		$count++;
		
		$line=~s/^>//;
		$id=$line;
		$fasta='';
	    }
	    
	}
    }
    #capture last sequence from file
    $seqs{$id}=$fasta;
    $count++;
    
    return \%seqs;
}


#takes list of genes and coords and makes a o a based on chromosome
# chr1 -> (id \t strand \t exonStarts \t exonEnds)
#
# Skips first line of infile
#
sub collapse_genes {
    my $in = shift;
    open (IN, "$in") or die "no infile $in given\n";

    my %genes; #hoa of genes by chromosome

    <IN>;
    while (my $line=<IN>) {
	chomp $line;
	my @info=split(/\t/, $line);
	my $chr=$info[0];
	my $gstart=$info[1];
	my $gend=$info[2];
	my $id=$info[3];
	my $strand=$info[5];
	my $enum=$info[9];
	my $elens=$info[10];
	my $estarts=$info[11];

	#convert starts into genomic coordinates
	my @starts=split(/,/, $estarts);
	my @lens=split(/,/, $elens);

	#starts
	my @gss;
	foreach my $start (@starts) {
	    my $ns=$start+$gstart;
	    push (@gss, $ns)
	}
	my $nslist;
	foreach my $ns (@gss) {
	    $nslist.="$ns,";
	}

	#ends
	my @ess;
	my $i="0";
	foreach my $ns (@gss) {
	    my $ne=$ns+$lens[$i];
	    push (@ess, $ne);
	    $i++;
	}
	my $nelist;
	foreach my $ne (@ess) {
	    $nelist.="$ne,";
	}   

	#print "$chr: $gstart, $gend\t$nslist\t$nelist\n";
	my $sum="$id\t$strand\t$nslist\t$nelist";

	push (@{$genes{$chr}}, $sum);

    }
    return \%genes;
}

#takes a hoa formatted gene list, opens a chromosome file and takes exon coords
# to make full length mRNAs.
sub make_seqs {
    my ($in, $gen)=@_;

    my @mrnas; # list of fasta mrna seqs.

    foreach my $chr (keys %{$in}) {
	my $fasta=$gen->{$chr};

	my $list=$in->{$chr}; #array ref to list of gene IDs/info on this chr
	my $arrayref=extract($list, $chr, $fasta);
	my @chrarray=@{$arrayref};
	push @mrnas, @chrarray;
    }

    return \@mrnas;
    
}


#called from make_seqs SUB
# reads chromosome and extracts mrna seq, via assembly of exon coords.
sub extract {
    my ($genes, $chr, $chrseq)=@_;

    my @mrnaseq; #to store .fa mRNA sequences.

    foreach my $id (@$genes) {
	my ($ucid, $strand, $sta, $stp)=split(/\t/, $id); #id, strand, starts, stops
	#print "$ucid, \n$strand, \n$starts, \n$stops\n";
	my @ostarts=split(/,/,$sta); 
	my @stops=split(/,/, $stp);

	#gtf file is 1 based, I am zero based, so subtract one from starts.
	# I think bed is zero, do not subtract
	my @starts;
	foreach my $st (@ostarts) {
	    #my $nst=$st-1;
	    my $nst=$st;
	    push (@starts, $nst);
	}
        

	my $i="0"; #counter to keep track of exons
	my $preseq;   #mrna sequence, pre revcom
	my $seq;      #final mrna sequence

	foreach my $coord (@starts) {
	    my $length=$stops[$i]-$coord;
	    my $startpos=$coord;
	    my $subseq=substr($chrseq, $startpos, $length);
	    $preseq.=$subseq;
	    $i++;
	}

	#reverse the complement if necessary
	if ($strand=~/-/) {
	    my $rev= reverse $preseq;
	    $rev=~ tr/ACGTacgt/TGCAtgca/;
	    $seq=$rev;
	} else {
	    $seq=$preseq;
	}

	#add to list
	my $entry=">$ucid\n$seq\n";
	push (@mrnaseq, $entry);
    }
    return \@mrnaseq;
}
    
#print out info to file
sub printout {
    my ($mrnas, $out)=@_;
    open (OUT, ">$out") or die "cant open outfile name\n";

    foreach my $mrna (@{$mrnas}) {
	print OUT "$mrna";
    }
}
