#!opt/bin/perl -w
use strict;
use POSIX;

#6/1/15
# select lncs with strands from ulitsky annotations

my ($in, $out)=@ARGV;

open (OUT, ">$out") or die "cant open outfile\n";
open (IN,"$in") or die "could not open in fasta file\n";

my $i;
my $j;

while (my $line=<IN>) {
    chomp $line;
    my @info=split(/\t/, $line);
    my $strand=$info[5];
    $i++;
    if ($strand ne ".") {
	print OUT "$line\n";
	$j++;
    }
}

print "$j stranded from $i total\n";
