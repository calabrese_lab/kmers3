# -*- coding: utf-8 -*-
"""

Description
-----------
Compares fragments between two seqs using pre-computed norm vectors.

Example
-------
python frag_query.py /path/to/bos_xist.fa /path/to/v22_xist.fa \
/path/to/v22_mean4.npy /path/to/v22_std4.npy \
/path/to/v22_bos_xist.txt 1000 -c 10 -s .25
"""

import argparse
import sys
import numpy as np
import pandas as pd

import kmer_counts
import pearson
import fasta

from scipy.stats import ttest_1samp

from fasta_reader import Reader
from my_tqdm import my_trange

class Analyzer(object):
    """Compares fragments between two seqs using pre-computed norm vectors.

    Parameters
    ----------
    infasta1 : str
        Path to the first fasta file. Only the first sequence will be used.
    infasta2 : str
        Path to the second fasta file. Only the first sequence will be used.
    norm : str
        Path to a binary numpy array to be used for the mean vector.
    std : str
        Path to a binary numpy array to be used for the standard deviation vector.
    outfile : str
        Path for output. Will be saved as a pandas DataFrame.
    frag_size : int
        Length of the fragments to be created.
    win_size : int (default=100)
        Number of basepairs to slide between each fragment.
    k : int (default=4)
        Size of kmer.
    N_limit : float (default=.1)
        Fraction of basepairs in a fragment that can be *N* before the sequence is thrown out.
    control : int (default=None)
        Number of iterations to run using randomly generated RNAs.
    smooth : float (default=None)
        Amount of smoothing to use during the creation of random sequences. 0 does no smoothing, 1 sets all bp probabilities to .25'
    silent : bool (default=False)
        Set to True to remove the progressbar
    leave : bool (default=True)
        Set to False to erase progressbar. Useful for running inside a loop.

    Parameters
    ----------
    counts1 : ndarray
        The normalized kmer counts for all fragments in infasta1.
    counts2 : ndarray
        The normalized kmer counts for all fragments in infasta2.
    pos : ndarray
        The start positions of each valid fragment in infasta1.
    pos2 : ndarray
        The start positions of each valid fragment in infasta2.
    top_dist : ndarray
        The highest scoring R-value between each fragment in infasta1 and all fragments in infasta2.
    top_pos : ndarray
        The position corresponding to the top_dist fragment (from pos2).
    """

    def __init__(self, infasta1=None, infasta2=None,
                 norm=None, std=None, outfile=None,
                 frag_size=None, win_size=100, k=4, N_limit=.1,
                 control=None, smooth=None, silent=False, leave=True):
        self.infasta1 = None
        if infasta1 is not None:
            self.seq1 = Reader(infasta1).get_seqs()[0]
        self.infasta2 = None
        if infasta2 is not None:
            self.seq2 = Reader(infasta2).get_seqs()[0]
        if norm is not None:
            self.norm = np.load(norm)
        if std is not None:
            self.std = np.load(std)
        self.outfile = outfile
        self.frag_size = frag_size
        self.win_size = win_size
        self.k = k
        self.N_limit = N_limit
        self.control = control
        self.smooth = smooth
        self.silent = silent
        self.leave = leave

        self.df = None
        self.counts1 = None
        self.counts2 = None
        self.pos = None
        self.pos2 = None
        self.top_dist = None
        self.top_pos = None

        self.frag_weights = None

    def normalize(self, counts):
        if self.norm is not None:
            counts -= self.norm
        if self.std is not None:
            counts /= self.std
        return counts

    def n_content(self, frag):
        ns = frag.count('N')
        return (ns/float(len(frag))) < self.N_limit

    def make_frags(self, seq):
        pos = []
        frags = []
        length = len(seq)
        for i in range(0, max(1, length-self.frag_size+1), self.win_size):
            frag = seq[i:i+self.frag_size]
            if self.n_content(frag):
                pos.append(i)
                frags.append(frag)
        pos = np.array(pos)
        return pos, frags

    def make_counts(self, frags):
        counter = kmer_counts.BasicCounter(k=self.k,
                                           centered=False,
                                           standardized=False,
                                           silent=self.silent)
        counter.seqs = frags
        counter.get_counts()
        counts = self.normalize(counter.counts)
        return counts

    def frag_and_count(self, seq):
        pos, frags = self.make_frags(seq)
        counts = self.make_counts(frags)
        return pos, counts

    def save(self):
        if self.outfile is not None:
            self.df.to_csv(self.outfile, sep='\t')

    def analyze_real(self):
        self.pos, self.counts1 = self.frag_and_count(self.seq1)
        self.pos2, self.counts2 = self.frag_and_count(self.seq2)
        self.top_pos = np.zeros(self.counts1.shape[0])
        self.top_dist = np.zeros(self.counts1.shape[0])
        length = len(self.top_dist)
        for i in range(length):
            single_row = self.counts1[i].reshape([1, self.counts1.shape[1]])
            dist = pearson.pearson(single_row, self.counts2)
            self.top_dist[i] = dist.max()
            self.top_pos[i] = self.pos2[dist.argmax()]
        data = np.vstack((self.top_dist, self.top_pos)).T
        self.df = pd.DataFrame(data, self.pos, ['R_value', 'Pos'])

    def smooth_weights(self, weights):
        """Smooth nucleotide weights according to the nhmmer method"""
        return [(self.smooth*.25)+((1-self.smooth)*w) for w in weights]

    def calc_frag_weights(self):
        """Calculate the nucleotide weights for each of the fragments"""
        pos, frags = self.make_frags(self.seq2)
        length = len(frags)
        self.frag_weights = np.zeros([length, 4])
        for i, frag in enumerate(frags):
            weights = fasta.RandomMaker().get_single_weights(frag)
            if self.smooth is not None:
                weights = self.smooth_weights(weights)
            self.frag_weights[i] = weights

    def make_rand_counts(self):
        bases = ['A', 'G', 'T', 'C']
        rand_frags = []
        for wt in self.frag_weights:
            frag = ''.join(np.random.choice(bases, self.frag_size, p=wt))
            rand_frags.append(frag)
        rand_counts = self.make_counts(rand_frags)
        return rand_counts

    def analyze_rand(self):
        self.calc_frag_weights()
        top_dist_rand = np.zeros([self.control, len(self.top_dist)])
        length = len(self.top_dist)
        for i in my_trange()(self.control, leave=self.leave):
            rand_counts = self.make_rand_counts()
            for j in range(length):
                single_row = self.counts1[j].reshape([1, self.counts1.shape[1]])
                dist = pearson.pearson(single_row, rand_counts)
                top_dist_rand[i, j] = dist.max()
        self.df['Rand Mean'] = top_dist_rand.mean(axis=0)
        self.df['Rand Std. Dev.'] = top_dist_rand.std(axis=0)
        p = [ttest_1samp(rand, r)[1] for rand, r in zip(top_dist_rand.T, self.df['R_value'])]
        self.df['p-val'] = p

    def run(self):
        #np.random.seed(0)
        self.analyze_real()
        if self.control is not None:
            self.analyze_rand()
        self.save()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(usage=__doc__,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('infasta1', type=str, help='Path to the first fasta file. Only the first sequence will be used.')
    parser.add_argument('infasta2', type=str, help='Path to the second fasta file. Only the first sequence will be used.')
    parser.add_argument('norm', type=str, help='Path to a binary numpy array to be used for the mean vector.')
    parser.add_argument('std', type=str, help='Path to a binary numpy array to be used for the standard deviation vector.')
    parser.add_argument('outfile', type=str, help='Path for output. Will be saved as a binary numpy array.')

    parser.add_argument('frag_size', type=int, help='The length of the fragments to be created.')
    parser.add_argument('-w', "--win_size", default=100, type=int, help='The number of basepairs to slide between each fragment.')
    parser.add_argument("-k", "--kmer", default=4, type=int, help='Size of kmer.')
    parser.add_argument("-n", "--N_limit", default=.1, type=float, help='The fraction of basepairs in a fragment that can be *N* before the sequence is thrown out.')
    parser.add_argument("-c", "--control", default=None, type=int, help='The number of random cycles to run.')
    parser.add_argument('-s', '--smooth', default=None, type=float, help='The amount of smoothing to use during the creation of random sequences. 0 does no smoothing, 1 sets all bp probabilities to .25')
    if len(sys.argv)==1:
        parser.print_help()
        sys.exit(0)
    args = parser.parse_args()
    if args.smooth is not None:
        assert 0 <= args.smooth <=1, 'If set, the smooth parameter must be between 0 and 1.'
    analyzer = Analyzer(args.infasta1,
                        args.infasta2,
                        args.norm,
                        args.std,
                        args.outfile,
                        args.frag_size,
                        args.win_size,
                        args.kmer,
                        args.N_limit,
                        args.control,
                        args.smooth)
    analyzer.run()
