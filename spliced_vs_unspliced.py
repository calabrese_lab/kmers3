"""

Examples
--------

For human:
common = '/home/jessime/Research/for_others/David/my_example/v26_full_unspliced_id2name.tab'
fc_base_path = '/home/jessime/Research/for_others/David/my_example/hepG2_cell_npa_FC_{}.tab'
lnc_un_ids = '/home/jessime/Research/for_others/David/my_example/v26_lnc_unspliced_transcript_ids.txt'
pc_un_ids = '/home/jessime/Research/for_others/David/my_example/v26_pc_unspliced_transcript_ids.txt'
lnc_sp_ids = '/home/jessime/Research/for_others/David/my_example/v26_lnc_spliced001_transcript_ids.txt'
pc_sp_ids = '/home/jessime/Research/for_others/David/my_example/v26_pc_spliced001_transcript_ids.txt'
pairs = '/home/jessime/Research/for_others/David/my_example/v26_spliced_unspliced_pairs.csv'
run(common, fc_base_path, lnc_un_ids, pc_un_ids, lnc_sp_ids, pc_sp_ids, pairs)

For mouse:
common = '/home/jessime/Research/for_others/David/M14_full_unspliced_ids_names.tab'
fc_base_path = '/home/jessime/Research/for_others/David/M14_FC_{}.tab'
lnc_un_ids = '/home/jessime/Research/mrna_lncrna/M14_lnc_unspliced_transcript_ids.txt'
pc_un_ids = '/home/jessime/Research/mrna_lncrna/M14_pc_unspliced_transcript_ids.txt'
lnc_sp_ids = '/home/jessime/Research/mrna_lncrna/M14_lnc_spliced201_transcript_ids.txt'
pc_sp_ids = '/home/jessime/Research/mrna_lncrna/M14_pc_spliced201_transcript_ids.txt'
pairs = '/home/jessime/Research/mrna_lncrna/M14_spliced_unspliced_pairs.csv'
run(common, fc_base_path, lnc_un_ids, pc_un_ids, lnc_sp_ids, pc_sp_ids, pairs)
"""

import numpy as np
import pandas as pd

import feature_counts

from my_tqdm import my_tqdm


def get_ids(path):
    with open(path) as infile:
        return [line.strip() for line in infile]


def normalize_and_label(infile, common):
    print(infile)
    fc = feature_counts.FeatureCounts(infile)
    unique_read_count = fc.get_assigned_reads()
    fc.normalize(unique_read_count=unique_read_count)
    fc.df.rename(columns={'Geneid': 'transcript_id'}, inplace=True)
    fc.df.set_index('transcript_id', inplace=True)

    df = fc.df.merge(common, left_index=True, right_index=True, validate='one_to_one')
    assert fc.df.shape[0] == df.shape[0]
    return df, unique_read_count


def normalize_and_label_unique(fc_base_path, common):
    infile = fc_base_path.format('unique')
    df, unique_read_count = normalize_and_label(infile, common)
    return df, unique_read_count


def normalize_and_label_multi(bf, common):
    infile = bf.format('multi')
    df, unique_read_count = normalize_and_label(infile, common)
    return df


def grab_unspliced(df, lnc_un_ids, pc_un_ids):
    df.reset_index(inplace=True)  # need numbers for the index
    lnc_un_ids = get_ids(lnc_un_ids)
    pc_un_ids = get_ids(pc_un_ids)
    un_ids = lnc_un_ids + pc_un_ids
    df = df.drop(columns=['gene_id', 'spliced'])
    df = df.rename(columns={'common': 'transcript_name'})
    unspliced_df = df[df['transcript_id'].isin(un_ids)].copy()
    return df, unspliced_df


def grab_spliced(df, lnc_sp_ids, pc_sp_ids):
    df.reset_index(inplace=True)  # need numbers for the index
    lnc_sp_ids = get_ids(lnc_sp_ids)
    pc_sp_ids = get_ids(pc_sp_ids)
    sp_ids = lnc_sp_ids + pc_sp_ids
    df = df.drop(columns=['gene_id', 'spliced'])
    df = df.rename(columns={'common': 'transcript_name'})
    spliced_df = df[df['transcript_id'].isin(sp_ids)].copy()
    return spliced_df


def calc_new_lengths(unspliced_df, unique_df):
    new_lengths = []
    for i, row in my_tqdm()(unspliced_df.iterrows()):
        # note that i is from the original index
        offset = int(row['Start'])
        unspliced_array = np.ones(int(row['End']) - offset + 1)
        base_name = '-'.join(row['transcript_name'].split('-')[:-1])
        j = i + 1

        # For each spliced transcript
        while True:
            next_row = unique_df.iloc[j]
            if next_row['transcript_name'][:-4] == base_name:
                exon_starts = next_row['Start'].split(';')
                exon_ends = next_row['End'].split(';')

                # Mask all the exons
                for start, end in zip(exon_starts, exon_ends):
                    start = int(start) - offset
                    end = int(end) - offset
                    unspliced_array[start: end] = 0
                j += 1
            else:
                break
        new_len = int(sum(unspliced_array))
        if new_len == 0:
            name = row['transcript_name']
            print(f'{name}: is 0 length')
        new_lengths.append(new_len)
    return new_lengths


def correct_len_and_norm(new_lengths, unspliced_df, unique_read_count):
    unspliced_df['old_length'] = unspliced_df['Length']
    unspliced_df['Length'] = new_lengths
    norm = 1E9/(unspliced_df['Length']*unique_read_count)
    unspliced_df['Norm'] = unspliced_df['Aligned.out.sam']*norm
    unspliced_df.drop(columns=['old_length'], inplace=True)
    return unspliced_df


def subtract_background_reads(unspliced_len,
                              unspliced_reads,
                              spliced_len,
                              spliced_reads,
                              old_norm,
                              unspliced_norm):
    unspliced_reads_per_base = unspliced_reads / unspliced_len
    spliced_reads_per_base = spliced_reads / spliced_len
    spliced_minus_unspliced_rpb = spliced_reads_per_base - unspliced_reads_per_base
    real_spliced_reads = spliced_minus_unspliced_rpb * spliced_len
    norm_factor = spliced_reads / old_norm
    new_spliced_norm = real_spliced_reads / norm_factor
    return real_spliced_reads, new_spliced_norm


def setup_unspliced_pairs(bf, pairs):
    unspliced_df = pd.read_csv(bf.format('unspliced'), index_col=0)
    unspliced_df.set_index('transcript_id', inplace=True)
    v26_spliced_paired = pd.read_csv(pairs, index_col=0)
    v26_spliced_paired = dict(zip(v26_spliced_paired['spliced_transcript'],
                              v26_spliced_paired['unspliced_transcript']))
    return v26_spliced_paired, unspliced_df


def correct_spliced_norm(spliced_df, bf, pairs):
    # Slow?
    v26_spliced_paired, unspliced_df = setup_unspliced_pairs(bf, pairs)
    spliced_reads_ls = []
    spliced_norms_ls = []
    for ix, spliced_row in spliced_df.iterrows():
        correct_unspliced = v26_spliced_paired[spliced_row['transcript_id']]
        unspliced_row = unspliced_df.loc[correct_unspliced]
        spliced_reads=spliced_row['Aligned.out.sam']
        if spliced_reads == 0:
            spliced_reads_ls.append(0)
            spliced_norms_ls.append(0)
            continue
        new_spliced_reads, new_spliced_norm = subtract_background_reads(
            unspliced_len=unspliced_row['Length'],
            unspliced_reads=unspliced_row['Aligned.out.sam'],
            spliced_len=spliced_row['Length'],
            spliced_reads=spliced_reads,
            old_norm=spliced_row['Norm'],
            unspliced_norm=unspliced_row['Norm'])
        spliced_reads_ls.append(new_spliced_reads)
        spliced_norms_ls.append(new_spliced_norm)
    spliced_df['Aligned.out.sam'] = spliced_reads_ls
    spliced_df['Norm'] = spliced_norms_ls
    return spliced_df


def run_unspliced(common, fc_base_path, lnc_un_ids, pc_un_ids):
    # common.set_index('transcript_id', inplace=True)
    unique_df, unique_read_count = normalize_and_label_unique(fc_base_path, common)
    unique_df, unspliced_df = grab_unspliced(unique_df, lnc_un_ids, pc_un_ids)
    new_lengths = calc_new_lengths(unspliced_df, unique_df)
    unspliced_df = correct_len_and_norm(new_lengths, unspliced_df, unique_read_count)
    outfile = fc_base_path.format('unspliced')
    unspliced_df.to_csv(outfile)
    print('-> ', outfile, '\n')


def run_spliced(common, fc_base_path, lnc_sp_ids, pc_sp_ids, pairs):
    multi_df = normalize_and_label_multi(fc_base_path, common)
    spliced_df = grab_spliced(multi_df, lnc_sp_ids, pc_sp_ids)
    spliced_df = correct_spliced_norm(spliced_df, fc_base_path, pairs)
    outfile = fc_base_path.format('spliced')
    spliced_df.to_csv(outfile)
    print('-> ', outfile, '\n')


def run(common, fc_base_path, lnc_un_ids, pc_un_ids, lnc_sp_ids, pc_sp_ids, pairs):
    common = pd.read_csv(common, index_col=0, sep='\t')
    run_unspliced(common, fc_base_path, lnc_un_ids, pc_un_ids)
    run_spliced(common, fc_base_path, lnc_sp_ids, pc_sp_ids, pairs)
