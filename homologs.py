# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 16:54:43 2018

@author: Jessime

Description
-----------
Compare two fasta file of potential homologs

Notes
-----
This is code to make data similar to Fig 1 of the paper.
"""
import numpy as np
from scipy.stats import ttest_ind

import alignment

from pearson import pearson
from kmer_counts import BasicCounter

class Comparisons:

    def __init__(self, infasta1=None, infasta2=None, seekr=True, stretcher=True, nhmmer=True,
                 counter_kwargs=None):
        self.infasta1 = infasta1
        self.infasta2 = infasta2
        self.seekr = seekr
        self.stretcher = stretcher
        self.nhmmer = nhmmer
        self.counter_kwargs = {} if counter_kwargs is None else counter_kwargs

        self.seekr_homologs = None
        self.seekr_non = None
        self.seekr_ttest = None
        self.seekr_detected = None

    def run_seekr(self):
        counts1 = BasicCounter(self.infasta1, **self.counter_kwargs).make_count_file()
        counts2 = BasicCounter(self.infasta2, **self.counter_kwargs).make_count_file()
        sim = pearson(counts1, counts2)

        self.seekr_homologs = np.diagonal(sim)
        index = np.triu_indices_from(sim, -1)
        self.seekr_non = sim[index]
        self.seekr_ttest = ttest_ind(self.seekr_homologs, self.seekr_non)
        self.seekr_detected = sum(row == col for row, col in zip(range(len(sim)), sim.argmax(1)))

    def run_nhmmer(self):
        pass

    def run(self):
        if self.seekr:
            self.run_seekr()
