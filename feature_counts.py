# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 15:00:42 2016

@author: jessime

"""

import pandas as pd
from numpy import nan

from fasta import Extracter

class FeatureCounts():
    """Performs commonly needed function for featureCount output files.

    Parameters
    ----------
    infile : str
        Path to featureCounts file
    verbose : bool (default=True)
        Reports naming failures if turned on.
    """
    def __init__(self, infile, verbose=True):
        self.infile = infile
        self.df = self._makeDF(infile)
        self.verbose = verbose

    def _makeDF(self, infile):
        return pd.read_csv(infile, sep='\t', skiprows=1)

    def _order_common_list(self, lookup):
        common_list = []
        for gid in self.df.Geneid:
            try:
                common_list.append(lookup[gid])
            except KeyError:
                if self.verbose:
                    print('Did not find key for {}.'.format(gid))
                common_list.append(nan)
        return common_list

    def geneid2common(self, infasta):
        extracter = Extracter(infasta)
        geneid = extracter.get_names('ensembl_gene')
        common = extracter.get_names('common')
        lookup = {k:v for k,v in zip(geneid, common)}
        common_list = self._order_common_list(lookup)
        self.df['Transcript'] = common_list

    def get_assigned_reads(self, path=None):
        """Get the number of assigned reads from the summary file.

        This is useful when you want to normalize to the number of unique reads.
        If your featureCount run allows multiple overlaps,
        summing the count column doesn't work.
        """
        if path is None:
            path = self.infile + '.summary'
        df = pd.read_csv(path, index_col=0, sep='\t')
        return df.loc['Assigned']['Aligned.out.sam']

    def normalize(self, unique_read_count=None):
        count_col = self.df.columns[-1]
        if unique_read_count is None:
            unique_read_count = self.df[count_col].sum()
        norm = 1E9/(self.df['Length']*unique_read_count)
        self.df['Norm'] = self.df[count_col]*norm
